package org.wcoomd.codomo.tester;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.wcoomd.dancer.springbean.ApplicationInit;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "/META-INF/applicationContext.xml" })
public class DancerTester {
	Logger logger = LoggerFactory.getLogger(DancerTester.class);

	@Autowired
	ApplicationInit applicationInit;

	@Test
	public void testCodomo() {
		applicationInit.initAll();
	}

}
