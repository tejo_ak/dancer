var routes={
		"home":{
			"display":"HOME",
			"path":"zk/content.zul"
		},
		"dancer_management":{
			"display":"DMR Management",
			"path":"zk/dancer_management.zul"
		},
		"dancer_decision":{
			"display":"DMR Decision",
			"path":"zk/dancer_decision.zul"
		},
		"dancer_editor":{
			"display":"My DMR",
			"path":"zk/dancer_editor.zul"
		},
		"dancer_view":{
			"display":"DMR Viewer",
			"path":"zk/dancer_view.zul"
		},
		"dancer_comment":{
			"display":"DMR Annotation",
			"path":"zk/dancer_comment.zul"
		},
		"dancer_decision_point":{
			"display":"DMR Decision Point",
			"path":"zk/dancer_decision_point.zul"
		},
		"code_manager":{
			"display":"Code Management",
			"path":"zk/code_manager.zul"
		},
		"document_view":{
			"display":"Document Viewer",
			"path":"zk/document_view.zul"
		},
		"profile_update":{
			"display":"Update profile",
			"path":"zk/profile_update.zul"
		}
};

var refresher=0;
var navigateRoute=function(id,qparam){
	qparam=(qparam)?qparam:{};
	refresher++;
	var path=routes[id].path+"?refresher=alwaysdiffernt"+refresher; 
	var args={"":qparam};
	var param=(args[""].param)?"?"+(args[""].param):"";
	args[""].path=path;
	args[""].id=id;
	var wgt=zk.Widget.$(jq("$inclMain"));
	zAu.send(new zk.Event(wgt, "onRoute", args, {toServer:true})); 
	var display=routes[id].display;
	jq("$menuSaatIni")[0].innerHTML=display;
	
}
var initApplication=function(){
	 
	var wgt=zk.Widget.$(jq("$inclMain"));
	zAu.send(new zk.Event(wgt, "onInitApplication", 'path', {toServer:true}));
}

function navigateSouthRoute(id,size,qparam){
	var path=routes[id].path; 
	var args={"":qparam};
	var param=(args[""].param)?"?"+(args[""].param):"";
	args[""].path=path;
	args[""].id=id;
    var widgetSouth = zk.Widget.$("$panelSouth"); //Using the pattern for $ + ID to select a ZK widget. 
    var wgt=zk.Widget.$(jq("$inclSouth"));
    size+=""
	if(size && size.indexOf("px")==-1){
    	size=size+"px"
    }
	widgetSouth.setHeight(size);
	if(!widgetSouth.isOpen()){
       try{
    	   widgetSouth.doClick_({
           domTarget:widgetSouth.$n('colled')  
         });
       }catch(e){ //ignore unhandled exception.
       }
     }
	 
	
	zAu.send(new zk.Event(wgt, "onRoute", args, {toServer:true}));
  }    

