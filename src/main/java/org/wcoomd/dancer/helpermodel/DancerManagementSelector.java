package org.wcoomd.dancer.helpermodel;

import org.wcoomd.dancer.model.Code;
import org.wcoomd.dancer.model.DancerStatus;
import org.wcoomd.dancer.model.MeetingSession;

public class DancerManagementSelector {
	private Boolean useSessionFiltering=false;
	private Boolean useOriginatorFiltering=false;
	private Boolean useStatusFiltering=false;
	private Boolean useKeywordFiltering;
	//
	private MeetingSession meetingSessionFIlter;
	private Code originatorFilter;
	private DancerStatus statusFilter;
	private String keywordFilter;
	//
	public Boolean getUseSessionFiltering() {
		return useSessionFiltering;
	}
	public void setUseSessionFiltering(Boolean useSessionFiltering) {
		this.useSessionFiltering = useSessionFiltering;
	}
	public Boolean getUseOriginatorFiltering() {
		return useOriginatorFiltering;
	}
	public void setUseOriginatorFiltering(Boolean useOriginatorFiltering) {
		this.useOriginatorFiltering = useOriginatorFiltering;
	}
	public Boolean getUseStatusFiltering() {
		return useStatusFiltering;
	}
	public void setUseStatusFiltering(Boolean useStatusFiltering) {
		this.useStatusFiltering = useStatusFiltering;
	}
	public Boolean getUseKeywordFiltering() {
		return useKeywordFiltering;
	}
	public void setUseKeywordFiltering(Boolean useKeywordFiltering) {
		this.useKeywordFiltering = useKeywordFiltering;
	}
	public MeetingSession getMeetingSessionFIlter() {
		return meetingSessionFIlter;
	}
	public void setMeetingSessionFIlter(MeetingSession meetingSessionFIlter) {
		this.meetingSessionFIlter = meetingSessionFIlter;
	}
	public Code getOriginatorFilter() {
		return originatorFilter;
	}
	public void setOriginatorFilter(Code originatorFilter) {
		this.originatorFilter = originatorFilter;
	}
	public DancerStatus getStatusFilter() {
		return statusFilter;
	}
	public void setStatusFilter(DancerStatus statusFilter) {
		this.statusFilter = statusFilter;
	}
	public String getKeywordFilter() {
		return keywordFilter;
	}
	public void setKeywordFilter(String keywordFilter) {
		this.keywordFilter = keywordFilter;
	}
	

}
