package org.wcoomd.dancer.viewmodel;

import java.util.ArrayList;
import java.util.List;

import org.wcoomd.dancer.model.Attachment;
import org.wcoomd.dancer.model.Dancer;
import org.wcoomd.dancer.model.User;
import org.wcoomd.dancer.springbean.AppUtil;
import org.wcoomd.dancer.springbean.CrudService;
import org.wcoomd.dancer.springbean.DancerService;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.QueryParam;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;

@VariableResolver(DelegatingVariableResolver.class)
public class ProfileUpdateVM {
	User edited;
	String passwordConfirm;
	String oldPassword;
	@WireVariable
	AppUtil appUtil;
	@WireVariable
	CrudService crudService;

	@Init
	public void init(@ExecutionArgParam("userId") String userId) {
		User tempUser = null;

		if (userId != null && !"".equals(userId)) {
			tempUser = crudService.querySingle(User.class, "select u from User u where u.id=%s", userId);
		}
		if (tempUser != null) {
			this.setEdited(tempUser);
		}

	}

	@NotifyChange({ "edited" })
	@Command
	public void save() {
		if (edited != null) {
			if (oldPassword != null && !"".equals(oldPassword)) {
				if (!oldPassword.equals(edited.getPassword())) {
					// password has been changed here
					if (edited.getPassword() != null && !edited.getPassword().equals(passwordConfirm)) {
						Clients.showNotification(
								"you attempt to change password but password confirmation doesn't match");
					}
				}
			}
			edited = crudService.save(edited);
			Clients.showNotification("your profile succesfully updated");
		}
	}

	public User getEdited() {
		return edited;
	}

	public void setEdited(User edited) {
		this.edited = edited;
	}

	public String getPasswordConfirm() {
		return passwordConfirm;
	}

	public void setPasswordConfirm(String passwordConfirm) {
		this.passwordConfirm = passwordConfirm;
	}

}
