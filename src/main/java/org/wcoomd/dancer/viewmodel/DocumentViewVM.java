package org.wcoomd.dancer.viewmodel;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.StringUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.AMedia;

public class DocumentViewVM {
	Logger logger = LoggerFactory.getLogger(DocumentViewVM.class);

	AMedia fileContent;
	String resource;

	@Init
	public void init(@ExecutionArgParam("resource") String resource,
			@ExecutionArgParam("resourceStream") String resourceStream) {
		logger.debug("resource and stream " + resource + " " + resourceStream);
		if (resource != null && !"".equals(resource)) {
			this.fileContent=null;
			this.resource = resource;
		}
		if (resourceStream != null && !"".equals(resourceStream)) {
			File f = new File(resourceStream);
			this.resource=null;
			if (f.exists()) {
				try {
					byte[] b = FileUtils.readFileToByteArray(f);
					String ext = StringUtils.getFilenameExtension(f.getName());
					fileContent = new AMedia("document", ext, "application/" + ext, b);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					throw new RuntimeException("error read file " + e.getMessage(), e);
				}
			}
		}

	}

	@NotifyChange({ "resource" })
	@AfterCompose
	public void afterCompose() {

	}

	public AMedia getFileContent() {
		return fileContent;
	}

	public void setFileContent(AMedia fileContent) {
		this.fileContent = fileContent;
	}

	public String getResource() {
		return resource;
	}

	public void setResource(String resource) {
		this.resource = resource;
	}

}
