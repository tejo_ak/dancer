package org.wcoomd.dancer.viewmodel;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wcoomd.dancer.model.Attachment;
import org.wcoomd.dancer.model.Dancer;
import org.wcoomd.dancer.springbean.AppUtil;
import org.wcoomd.dancer.springbean.CrudService;
import org.wcoomd.dancer.springbean.DancerService;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.bind.annotation.QueryParam;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;

@VariableResolver(DelegatingVariableResolver.class)
public class DancerViewVM {
	Logger logger = LoggerFactory.getLogger(DancerViewVM.class);
	Dancer edited;
	List<Attachment> attachmentList;
	@WireVariable
	AppUtil appUtil;
	@WireVariable
	CrudService crudService;

	@WireVariable
	DancerService dancerService;

	@Init
	public void init(@QueryParam("dancer_id") String dancer_id, @ExecutionArgParam("dancerId") String dancerId) {
		logger.debug("ini adlaah on load " + dancer_id + " " + dancerId);
		Dancer tempDancer = null;
		if (dancer_id != null && !"".equals(dancer_id)) {
			tempDancer = dancerService.getDancer(new Long(dancer_id));
		}
		if (dancerId != null && !"".equals(dancerId)) {
			tempDancer = dancerService.getDancer(new Long(dancerId));
		}
		logger.debug("tempDancer:" + tempDancer);
		if (tempDancer != null) {
			this.setEdited(tempDancer);
			attachmentList = attachmentOfDancer();
		}

	}

	public List<Attachment> attachmentOfDancer() {
		if (edited == null || edited.getId() == null) {
			return new ArrayList<>();
		}
		return crudService.query(Attachment.class, "select a from Attachment a where a.dancer.id=%s",
				edited.getId().toString());
	}

	public Dancer getEdited() {
		return edited;
	}

	public void setEdited(Dancer edited) {
		this.edited = edited;
	}

	public List<Attachment> getAttachmentList() {
		return attachmentList;
	}

	public void setAttachmentList(List<Attachment> attachmentList) {
		this.attachmentList = attachmentList;
	}

}
