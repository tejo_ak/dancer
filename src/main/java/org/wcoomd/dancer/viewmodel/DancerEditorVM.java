package org.wcoomd.dancer.viewmodel;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.asm.commons.TryCatchBlockSorter;
import org.wcoomd.dancer.model.Attachment;
import org.wcoomd.dancer.model.Code;
import org.wcoomd.dancer.model.Dancer;
import org.wcoomd.dancer.model.DancerStatus;
import org.wcoomd.dancer.model.MeetingSession;
import org.wcoomd.dancer.model.Trail;
import org.wcoomd.dancer.model.User;
import org.wcoomd.dancer.springbean.AppUtil;
import org.wcoomd.dancer.springbean.ApplicationConstant;
import org.wcoomd.dancer.springbean.CrudService;
import org.wcoomd.dancer.springbean.DancerService;
import org.wcoomd.dancer.springbean.PagingModel;
import org.wcoomd.dancer.springbean.Trailler;
import org.zkoss.bind.BindContext;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.util.media.Media;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.event.UploadEvent;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;

@VariableResolver(DelegatingVariableResolver.class)
public class DancerEditorVM {
	Logger logger = LoggerFactory.getLogger(DancerEditorVM.class);
	Dancer edited;
	Dancer selected;
	//
	boolean deletable;
	boolean savable;
	boolean copyable;
	boolean submitable;

	PagingModel pagingModel = new PagingModel(6);
	List<Dancer> dancerList;
	List<Attachment> attachmentList;
	// Filter
	List<MeetingSession> sessionFilterCombo;
	MeetingSession sessionFilterComboSelected;
	boolean useSesionFilter;
	//
	String keyword;
	//
	List<Code> statusFilterCombo;
	Code statusFilterComboSelected;
	boolean useStatusFilter;
	//
	// originator combo
	List<Code> originatorCombo;
	Code originatorComboSelected;
	// datasetCombo
	List<Code> datasetCombo;
	Code datasetComboSelected;
	// procedureCombo
	List<Code> procedureCombo;
	Code procedureComboSelected;
	// sessionCombo
	List<MeetingSession> sessionCombo;
	MeetingSession sessionComboSelected;
	// artefactTypeCombo
	List<Code> artefactTypeCombo;
	Code artefactTypeComboSelected;
	// updateTypeCombo
	List<Code> updateTypeCombo;
	Code updateTypeComboSelected;
	//
	@WireVariable
	CrudService crudService;

	@WireVariable
	DancerService dancerService;

	@WireVariable
	AppUtil appUtil;

	@WireVariable
	Trailler trailler;

	@AfterCompose
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
	}

	@Init
	public void init() {
		edited = new Dancer();
		originatorCombo = getCodeComboList(ApplicationConstant.CODE_TYPE_ORIGINATOR);
		datasetCombo = getCodeComboList(ApplicationConstant.CODE_TYPE_DATASET);
		procedureCombo = getCodeComboList(ApplicationConstant.CODE_TYPE_PROCEDURE);
		artefactTypeCombo = getCodeComboList(ApplicationConstant.CODE_TYPE_ARTEFACT);
		updateTypeCombo = getCodeComboList(ApplicationConstant.CODE_TYPE_UPDATE);
		statusFilterCombo = getCodeComboList(ApplicationConstant.CODE_TYPE_STATUS);
		sessionFilterCombo = crudService.query(MeetingSession.class, String.format("select c from MeetingSession c  "));
		sessionCombo = crudService.query(MeetingSession.class, String.format("select c from MeetingSession c  "));
	};

	private void checkCrudButtonStatus() {
		savable = (edited != null
				&& ((edited.getStatus() != null && String.format("%s", ApplicationConstant.CODE_STATUS_TYPE_DRAFT)
						.contains(edited.getStatus().getCode().getCode()))
				|| (edited.getStatus() == null && edited.getId() == null)));
		submitable = savable && edited.getStatus() != null;
		copyable = (edited != null && (edited.getStatus() != null && String
				.format("%s-%s-%s-%s-%s", ApplicationConstant.CODE_STATUS_TYPE_DRAFT,
						ApplicationConstant.CODE_STATUS_TYPE_WITHDRAWN, ApplicationConstant.CODE_STATUS_TYPE_SUBMITED,
						ApplicationConstant.CODE_STATUS_TYPE_APPROVED, ApplicationConstant.CODE_STATUS_TYPE_DEFFERED)
				.contains(edited.getStatus().getCode().getCode())));
		deletable = (edited != null && (edited.getStatus() != null && String
				.format("%s-%s-%s", ApplicationConstant.CODE_STATUS_TYPE_DRAFT,
						ApplicationConstant.CODE_STATUS_TYPE_WITHDRAWN, ApplicationConstant.CODE_STATUS_TYPE_DEFFERED)
				.contains(edited.getStatus().getCode().getCode())));
	}

	@NotifyChange({ "edited", "dancerList", "savable", "copyable", "deletable", "submitable", "pagingModel" })
	@Command
	public void save() {
		try {
			edited = crudService.save(edited);
			trailler.trailingStatus(null, edited, ApplicationConstant.CODE_STATUS_TYPE_DRAFT, "editing",
					Trailler.TRAIL_TYPE_CODE_UPDATE);
			edited = dancerService.getDancer(edited.getId());
			Clients.showNotification(String.format("Your DMR  is saved successfully! %s", ""));
			checkCrudButtonStatus();
			pagingModel.initialData(edited);
			dancerList = paging();
		} catch (Exception e) {
			String erme = String.format("error saving the DMR because %s", e.getMessage());
			logger.error(erme, e);
			throw new RuntimeException(erme, e);
		}
	}

	@NotifyChange({ "edited", "dancerList" })
	@Command
	public void submit() {
		Messagebox.show(
				String.format("The DMR with WCO LOG %s (%s) will be submitted. A submited DMR is no longer savable!",
						edited.getId(), edited.getUserReference()),
				"Confirm submission", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
				new EventListener<Event>() {

					@Override
					public void onEvent(Event event) throws Exception {
						// TODO Auto-generated method stub
						if (event != null && "onOK".equals(event.getName())) {
							try {
								trailler.trailingStatus(null, edited, ApplicationConstant.CODE_STATUS_TYPE_SUBMITED,
										"end editing", Trailler.TRAIL_TYPE_CODE_UPDATE);
								edited = dancerService.getDancer(edited.getId());
								dancerList = paging();
								checkCrudButtonStatus();
								Clients.showNotification("DMR succesfully submited");
								BindUtils.postNotifyChange(null, null, DancerEditorVM.this, "edited");
								BindUtils.postNotifyChange(null, null, DancerEditorVM.this, "dancerList");
								BindUtils.postNotifyChange(null, null, DancerEditorVM.this, "savable");
								BindUtils.postNotifyChange(null, null, DancerEditorVM.this, "copyable");
								BindUtils.postNotifyChange(null, null, DancerEditorVM.this, "submitable");
								BindUtils.postNotifyChange(null, null, DancerEditorVM.this, "deletable");
							} catch (Exception e) {
								String erme = String.format("error submiting the DMR because %s", e.getMessage());
								logger.error(erme, e);
								throw new RuntimeException(erme, e);
							}
						}

					}
				});

	}

	@NotifyChange({ "edited", "savable", "deletable" })
	@Command
	public void newDancer() {
		edited = new Dancer();
		User us = crudService.querySingle(User.class, "select u from User u where u.id=%s", Executions.getCurrent()
				.getSession().getAttribute(ApplicationConstant.USER_SESSION_ATTRIBUTE).toString());
		edited.setSubmitter(us);
		savable = true;
	}

	@NotifyChange({ "edited", "dancerList", "savable", "copyable", "deletable", "submitable", "pagingModel" })
	@Command
	public void savethennew() {
		try {
			save();
			edited = null;
			edited = new Dancer();
			checkCrudButtonStatus();
			User us = crudService.querySingle(User.class, "select u from User u where u.id=%s", Executions.getCurrent()
					.getSession().getAttribute(ApplicationConstant.USER_SESSION_ATTRIBUTE).toString());
			edited.setSubmitter(us);

			// dancerList = paging();
		} catch (Exception e) {
			String erme = String.format("error saving the DMR because %s", e.getMessage());
			logger.error(erme, e);
			throw new RuntimeException(erme, e);
		}
	}

	@NotifyChange({ "edited", "dancerList", "pagingModel" })
	@Command
	public void delete() {
		Messagebox.show(
				String.format("The DMR with WCO LOG %s (%s) will be deleted", edited.getId(),
						edited.getUserReference()),
				"Confirm deletion", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new EventListener<Event>() {

					@Override
					public void onEvent(Event event) throws Exception {
						// TODO Auto-generated method stub
						if (event != null && "onOK".equals(event.getName())) {
							try {
								DancerStatus st = edited.getStatus();
								edited.setStatus(null);
								edited.setSupportBuffer(null);
								crudService.save(edited);
								crudService.execute(
										"update  DancerStatus t set t.trail=null,t.dancer=null where t.dancer.id=%s",
										"" + edited.getId());
								crudService.execute("delete  Trail t where t.dancerStatus.id=%s", "" + st.getId());
								crudService.execute("delete  DancerStatus t where t.dancer.id=%s", "" + edited.getId());
								crudService.delete(edited);
								pagingModel.setInitialDataId(edited.getId());
								edited = null;
								// dancerList =
								// crudService.getAll(Dancer.class);
								dancerList = paging();
								checkCrudButtonStatus();
								Clients.showNotification("DMR succesfully deleted");
								BindUtils.postNotifyChange(null, null, DancerEditorVM.this, "edited");
								BindUtils.postNotifyChange(null, null, DancerEditorVM.this, "pagingModel");
								BindUtils.postNotifyChange(null, null, DancerEditorVM.this, "dancerList");
								BindUtils.postNotifyChange(null, null, DancerEditorVM.this, "savable");
								BindUtils.postNotifyChange(null, null, DancerEditorVM.this, "copyable");
								BindUtils.postNotifyChange(null, null, DancerEditorVM.this, "submitable");
								BindUtils.postNotifyChange(null, null, DancerEditorVM.this, "deletable");
							} catch (Exception e) {
								String erme = String.format("error deleting  DMR because %s", e.getMessage());
								logger.error(erme, e);
								throw new RuntimeException(erme, e);
							}
						}

					}
				});

	}

	public List<Code> getCodeComboList(String typeCode) {
		return crudService.query(Code.class, "select c from Code c where c.type.code='%s'", typeCode);
	}

	@NotifyChange({ "dancerList", "pagingModel" })
	@Command
	public List<Dancer> search() {
		// try {
		String sessionFilter = (useSesionFilter && sessionFilterComboSelected != null)
				? String.format(" and c.session.id=%s", sessionFilterComboSelected.getId()) : "";
		String statusFilter = (useStatusFilter && statusFilterComboSelected != null)
				? String.format(" and c.status.code.id=%s", statusFilterComboSelected.getId()) : "";
		String keywordFilter = (keyword != null && !"".equals(keyword)) ? String.format(
				"and (c.businessNeed like '%%%s%%' or c.purpose like '%%%s%%' or c.untdedDefinition like '%%%s%%')",
				keyword, keyword, keyword) : "";
		String order = " order by c.status.trail.activityAt desc";
		String preSearchPaging = String.format(" and c.id=%s", pagingModel.getInitialDataId());
		preSearchPaging = (pagingModel.getInitialDataId() != null) ? preSearchPaging : "";
		dancerList = crudService.query(Dancer.class,
				pagingModel.query("select c", "select count(*)", " from Dancer c ",
						" LEFT JOIN FETCH c.submitter LEFT JOIN FETCH c.originator LEFT JOIN FETCH c.status st LEFT JOIN FETCH st.code "
								+ " LEFT JOIN FETCH st.trail LEFT JOIN FETCH c.session ",
						" where 1=1 %s %s %s %s", order, sessionFilter, statusFilter, keywordFilter, preSearchPaging)
				.count());
		pagingModel.setInitialDataId(null);
		logger.debug(String.format("query search editor = %s", pagingModel.getQuery()));
		return dancerList;
		// } catch (Exception e) {
		// dancerList = null;
		// Clients.evalJavaScript("alert('error search dancer:'+" +
		// e.getMessage() + ")");
		// return new ArrayList<>();
		// }
	}

	@NotifyChange({ "dancerList", "pagingModel" })
	@Command
	public List<Dancer> paging() {
		if (!pagingModel.initiated()) {
			dancerList = search();
			return dancerList;
		}
		dancerList = crudService.query(Dancer.class, pagingModel.dontCount());
		return dancerList;
	}

	@NotifyChange({ "edited", "originatorComboSelected", "savable", "copyable", "deletable", "submitable",
			"attachmentList" })
	@Command
	public void select() {
		if (this.selected != null) {
			edited = dancerService.getDancer(this.selected.getId());
			// originatorComboSelected = edited.getOriginator();
			checkCrudButtonStatus();
			attachmentList = attachmentOfDancer();
			logger.debug("attachmentlist " + attachmentList.size());
		}

	}

	@NotifyChange({ "edited", "savable", "copyable", "deletable", "submitable" })
	@Command
	public void copy() {
		if (edited != null && edited.getId() != null) {
			try {
				edited.setId(null);
				edited.setStatus(null);
				checkCrudButtonStatus();
			} catch (Exception e) {
				String erme = String.format("error copying  DMR because %s", e.getMessage());
				logger.error(erme, e);
				throw new RuntimeException(erme, e);
			}
		}
	}

	@NotifyChange({ "attachmentList" })
	@Command
	public void upload(BindContext ctx) {
		UploadEvent event = (UploadEvent) ctx.getTriggerEvent();
		Media m = event.getMedia();
		File folder = new File(System.getProperty("user.home") + "/dancer_resources");
		File file = new File(folder, m.getName());
		if (m.isBinary()) {
			InputStream is = m.getStreamData();
			try {
				if (!folder.exists()) {
					folder.mkdirs();
				}
				if (!file.exists()) {
					file.exists();
				}
				FileUtils.copyInputStreamToFile(is, file);
				Attachment at = new Attachment();
				at.setDancer(edited);
				at.setPath(file.getAbsolutePath());
				at.setName(m.getName());
				crudService.save(at);
				attachmentList = attachmentOfDancer();
				logger.debug("file to save" + m.getName());
			} catch (IOException e) {
				String erme = String.format("error uploading attachment for the DMR because %s", e.getMessage());
				logger.error(erme, e);
				throw new RuntimeException(erme, e);
			}
		}
	}

	@NotifyChange({ "attachmentList" })
	@Command
	public void deleteAttachment(@BindingParam("attachmentId") String attachmentId) {
		if (attachmentId != null && !"".equals(attachmentId)) {
			Messagebox.show(
					String.format("The selected attachment will be deleted", edited.getId(), edited.getUserReference()),
					"Confirm Attachment deletion", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION,
					new EventListener<Event>() {

						@Override
						public void onEvent(Event event) throws Exception {
							// TODO Auto-generated method stub
							if (event != null && "onOK".equals(event.getName())) {
								List<Attachment> ats = crudService.query(Attachment.class,
										"select c from Attachment c where c.id=%s", attachmentId);
								if (ats.size() > 0) {
									try {
										FileUtils.forceDelete(new File(ats.get(0).getPath()));
									} catch (IOException e) {
										throw new RuntimeException("error delet file: " + e.getMessage(), e);
									}
									crudService.execute("delete Attachment c where c.id=%s", attachmentId);
									attachmentList = attachmentOfDancer();
									Clients.showNotification("attachment file deleted successfully");
								}
								BindUtils.postNotifyChange(null, null, DancerEditorVM.this, "attachmentList");

							}
						}
					});
		}
	}

	public List<Attachment> attachmentOfDancer() {
		if (edited == null || edited.getId() == null) {
			return new ArrayList<>();
		}
		return crudService.query(Attachment.class, "select a from Attachment a where a.dancer.id=%s",
				edited.getId().toString());
	}

	public String concat(String... text) {

		return appUtil.concat(text);
	}

	public Dancer getEdited() {
		return edited;
	}

	public void setEdited(Dancer edited) {
		this.edited = edited;
	}

	public List<Dancer> getDancerList() {
		return dancerList;
	}

	public void setDancerList(List<Dancer> dancerList) {
		this.dancerList = dancerList;
	}

	public Dancer getSelected() {
		return selected;
	}

	public void setSelected(Dancer selected) {
		this.selected = selected;
	}

	public List<Code> getOriginatorCombo() {
		return originatorCombo;
	}

	public void setOriginatorCombo(List<Code> originatorCombo) {
		this.originatorCombo = originatorCombo;
	}

	public Code getOriginatorComboSelected() {
		return originatorComboSelected;
	}

	public void setOriginatorComboSelected(Code originatorComboSelected) {
		this.originatorComboSelected = originatorComboSelected;
	}

	public List<Code> getDatasetCombo() {
		return datasetCombo;
	}

	public void setDatasetCombo(List<Code> datasetCombo) {
		this.datasetCombo = datasetCombo;
	}

	public Code getDatasetComboSelected() {
		return datasetComboSelected;
	}

	public void setDatasetComboSelected(Code datasetComboSelected) {
		this.datasetComboSelected = datasetComboSelected;
	}

	public List<Code> getProcedureCombo() {
		return procedureCombo;
	}

	public void setProcedureCombo(List<Code> procedureCombo) {
		this.procedureCombo = procedureCombo;
	}

	public Code getProcedureComboSelected() {
		return procedureComboSelected;
	}

	public void setProcedureComboSelected(Code procedureComboSelected) {
		this.procedureComboSelected = procedureComboSelected;
	}

	public List<Code> getArtefactTypeCombo() {
		return artefactTypeCombo;
	}

	public void setArtefactTypeCombo(List<Code> artefactTypeCombo) {
		this.artefactTypeCombo = artefactTypeCombo;
	}

	public Code getArtefactTypeComboSelected() {
		return artefactTypeComboSelected;
	}

	public void setArtefactTypeComboSelected(Code artefactTypeComboSelected) {
		this.artefactTypeComboSelected = artefactTypeComboSelected;
	}

	public List<Code> getUpdateTypeCombo() {
		return updateTypeCombo;
	}

	public void setUpdateTypeCombo(List<Code> updateTypeCombo) {
		this.updateTypeCombo = updateTypeCombo;
	}

	public Code getUpdateTypeComboSelected() {
		return updateTypeComboSelected;
	}

	public void setUpdateTypeComboSelected(Code updateTypeComboSelected) {
		this.updateTypeComboSelected = updateTypeComboSelected;
	}

	public PagingModel getPagingModel() {
		return pagingModel;
	}

	public void setPagingModel(PagingModel pagingModel) {
		this.pagingModel = pagingModel;
	}

	public List<MeetingSession> getSessionFilterCombo() {
		return sessionFilterCombo;
	}

	public void setSessionFilterCombo(List<MeetingSession> sessionFilterCombo) {
		this.sessionFilterCombo = sessionFilterCombo;
	}

	public MeetingSession getSessionFilterComboSelected() {
		return sessionFilterComboSelected;
	}

	public void setSessionFilterComboSelected(MeetingSession sessionFilterComboSelected) {
		this.sessionFilterComboSelected = sessionFilterComboSelected;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public List<Code> getStatusFilterCombo() {
		return statusFilterCombo;
	}

	public void setStatusFilterCombo(List<Code> statusFilterCombo) {
		this.statusFilterCombo = statusFilterCombo;
	}

	public Code getStatusFilterComboSelected() {
		return statusFilterComboSelected;
	}

	public void setStatusFilterComboSelected(Code statusFilterComboSelected) {
		this.statusFilterComboSelected = statusFilterComboSelected;
	}

	public boolean isUseSesionFilter() {
		return useSesionFilter;
	}

	public void setUseSesionFilter(boolean useSesionFilter) {
		this.useSesionFilter = useSesionFilter;
	}

	public boolean isUseStatusFilter() {
		return useStatusFilter;
	}

	public void setUseStatusFilter(boolean useStatusFilter) {
		this.useStatusFilter = useStatusFilter;
	}

	public List<MeetingSession> getSessionCombo() {
		return sessionCombo;
	}

	public void setSessionCombo(List<MeetingSession> sessionCombo) {
		this.sessionCombo = sessionCombo;
	}

	public MeetingSession getSessionComboSelected() {
		return sessionComboSelected;
	}

	public void setSessionComboSelected(MeetingSession sessionComboSelected) {
		this.sessionComboSelected = sessionComboSelected;
	}

	public boolean isDeletable() {
		return deletable;
	}

	public void setDeletable(boolean deletable) {
		this.deletable = deletable;
	}

	public boolean isSavable() {
		return savable;
	}

	public void setSavable(boolean savable) {
		this.savable = savable;
	}

	public boolean isCopyable() {
		return copyable;
	}

	public void setCopyable(boolean copyable) {
		this.copyable = copyable;
	}

	public boolean isSubmitable() {
		return submitable;
	}

	public void setSubmitable(boolean submitable) {
		this.submitable = submitable;
	}

	public List<Attachment> getAttachmentList() {
		return attachmentList;
	}

	public void setAttachmentList(List<Attachment> attachmentList) {
		this.attachmentList = attachmentList;
	}

}
