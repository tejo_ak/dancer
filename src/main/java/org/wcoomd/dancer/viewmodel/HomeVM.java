package org.wcoomd.dancer.viewmodel;

import java.util.ArrayList;
import java.util.List;

import org.wcoomd.dancer.model.Annotation;
import org.wcoomd.dancer.model.Dancer;
import org.wcoomd.dancer.springbean.ApplicationConstant;
import org.wcoomd.dancer.springbean.CrudService;
import org.wcoomd.dancer.springbean.PagingModel;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;

@VariableResolver(DelegatingVariableResolver.class)
public class HomeVM {
	@WireVariable
	private CrudService crudService;

	private List<Annotation> commentList = new ArrayList<>();
	private List<Dancer> dancerList = new ArrayList<>();
	private PagingModel pagingModel = new PagingModel(10);

	private String statOriginator;
	private String statSubmited;
	private String statApproved;

	@Init
	public void init() {
		pagingModel.setCurentPage(1);
		commentList = crudService.query(Annotation.class, pagingModel.query("select a ", "select count(*)",
				"from Annotation a", " LEFT JOIN FETCH a.trail t LEFT JOIN FETCH t.activityBy  ", "", "").count());
		dancerList = crudService.query(Dancer.class,
				pagingModel
						.query("select c ", "select count(*)", "from Dancer c",
								" LEFT JOIN FETCH c.submitter LEFT JOIN FETCH c.originator LEFT JOIN FETCH c.status st LEFT JOIN FETCH st.trail tl LEFT JOIN FETCH st.code "
										+ " LEFT JOIN FETCH c.session ",
								"where 1=1 order by tl.activityAt desc", "")
						.dontCount());
	}

	@NotifyChange({ "statApproved", "statSubmited", "statOriginator" })
	@Command
	public void stat() {
		Long lStatOriginator = crudService.querySingle(Long.class,
				"select count(c) from Code c where exists (select d from Dancer d where d.originator.id=c.id)");
		Long lStatSubmited = crudService.querySingle(Long.class,
				"select count(c) from Dancer c where c.status.code.code='%s'",
				ApplicationConstant.CODE_STATUS_TYPE_SUBMITED);
		Long lStatApproved = crudService.querySingle(Long.class,
				"select count(c) from Dancer c where c.status.code.code='%s'",
				ApplicationConstant.CODE_STATUS_TYPE_APPROVED);
		this.statApproved = lStatApproved.toString();
		this.statSubmited = lStatSubmited.toString();
		this.statOriginator = lStatOriginator.toString();

	}

	public List<Annotation> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<Annotation> commentList) {
		this.commentList = commentList;
	}

	public List<Dancer> getDancerList() {
		return dancerList;
	}

	public void setDancerList(List<Dancer> dancerList) {
		this.dancerList = dancerList;
	}

	public String getStatOriginator() {
		return statOriginator;
	}

	public void setStatOriginator(String statOriginator) {
		this.statOriginator = statOriginator;
	}

	public String getStatSubmited() {
		return statSubmited;
	}

	public void setStatSubmited(String statSubmited) {
		this.statSubmited = statSubmited;
	}

	public String getStatApproved() {
		return statApproved;
	}

	public void setStatApproved(String statApproved) {
		this.statApproved = statApproved;
	}

}
