package org.wcoomd.dancer.viewmodel;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wcoomd.dancer.model.Annotation;
import org.wcoomd.dancer.model.Dancer;
import org.wcoomd.dancer.model.User;
import org.wcoomd.dancer.springbean.AppUtil;
import org.wcoomd.dancer.springbean.ApplicationConstant;
import org.wcoomd.dancer.springbean.CrudService;
import org.wcoomd.dancer.springbean.DancerService;
import org.wcoomd.dancer.springbean.Trailler;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;

@VariableResolver(DelegatingVariableResolver.class)
public class DancerCommentVM {
	Logger logger = LoggerFactory.getLogger(DancerCommentVM.class);
	private Annotation newComment;
	private List<Annotation> commentList;
	private Dancer dancer;

	@WireVariable
	CrudService crudService;

	@WireVariable
	Trailler trailler;

	@WireVariable
	AppUtil appUtil;

	@WireVariable
	DancerService dancerService;

	@Init()
	public void init(@ExecutionArgParam("dancerId") String dancerId) {
		if (dancerId != null) {
			dancer = dancerService.getDancer(dancerId);
			commentList = revealOfDancer(dancer);
			newComment = new Annotation();
		}
	}

	@NotifyChange({ "commentList", "newComment" })
	@Command
	public void save() {
		if (newComment != null && dancer != null) {
			if (newComment.getId() == null) {
				List<User> us = crudService.query(User.class, "select u from User u where u.id=%s", Executions
						.getCurrent().getSession().getAttribute(ApplicationConstant.USER_SESSION_ATTRIBUTE).toString());
				newComment.setDancer(dancer);
				trailler.trailingAnnotation(us.size() > 0 ? us.get(0) : null, newComment,
						Trailler.TRAIL_TYPE_CODE_CREATE);
				commentList = revealOfDancer(dancer);

				logger.debug("dancer: " + dancer);
				logger.debug("revealed comment list: " + commentList.size());
			}

			newComment = new Annotation();
		}
	}

	public List<Annotation> revealOfDancer(Dancer dancer) {
		if (dancer != null && dancer.getId() != null) {
			logger.debug("dancer is exists to determine the annotation query");
			return crudService.query(Annotation.class,
					String.format(
							"select a from Annotation a LEFT JOIN FETCH a.trail t LEFT JOIN FETCH t.activityBy where a.dancer.id=%s",
							dancer.getId()));
		}
		return new ArrayList<>();
	}

	public Annotation getNewComment() {
		return newComment;
	}

	public void setNewComment(Annotation newComment) {
		this.newComment = newComment;
	}

	public List<Annotation> getCommentList() {
		return commentList;
	}

	public void setCommentList(List<Annotation> commentList) {
		this.commentList = commentList;
	}

	public Dancer getDancer() {
		return dancer;
	}

	public void setDancer(Dancer dancer) {
		this.dancer = dancer;
	}

}
