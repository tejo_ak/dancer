package org.wcoomd.dancer.viewmodel;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wcoomd.dancer.helpermodel.DancerManagementSelector;
import org.wcoomd.dancer.model.Code;
import org.wcoomd.dancer.model.Dancer;
import org.wcoomd.dancer.model.MeetingSession;
import org.wcoomd.dancer.springbean.AppUtil;
import org.wcoomd.dancer.springbean.CrudService;
import org.wcoomd.dancer.springbean.PagingModel;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;

@VariableResolver(DelegatingVariableResolver.class)
public class DancerManagementVM {
	Logger logger = LoggerFactory.getLogger(DancerManagementVM.class);
	PagingModel pagingModel=new PagingModel(10);

	Dancer selected;
	List<Dancer> dancerList;
	DancerManagementSelector managementSelector;
	List<MeetingSession> sessionCombo;
	MeetingSession sessionComboSelected;
	List<Code> originatorCombo;
	Code originatorComboSelected;
	List<Code> statusCombo;
	Code statusComboSelected;

	@WireVariable
	CrudService crudService;

	@WireVariable
	AppUtil appUtil;
	// @AfterCompose
	// public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
	// Selectors.wireComponents(view, this, false);
	// //crudService = (CrudService) SpringUtil.getBean("crudService");
	// }

	@Init
	public void init() {
		managementSelector = new DancerManagementSelector();
		originatorCombo = crudService.query(Code.class,
				String.format("select c from Code c  JOIN FETCH c.type where c.type.code='DANCER_ORIGINATOR'"));
		statusCombo = crudService.query(Code.class,
				String.format(
						"select c from Code c  JOIN FETCH c.type where c.type.code='DANCER_STATUS_TYPE' and '%s' like concat('%%',c.code,'%%')",
						"SUBMITED-APPROVED-WITHDRAWN"));
		sessionCombo = crudService.query(MeetingSession.class, String.format("select c from MeetingSession c  "));
	}

	public String maxCombine(Long max, String... words) {
		return appUtil.maxCombine(max, words);
	}

	@NotifyChange({ "dancerList","managementSelector","pagingModel" })
	@Command
	public List<Dancer> search() {
		String q = "";
		
		String keyword = managementSelector.getKeywordFilter();
		// String where = (managementSelector.getKeywordFilter() != null
		// && !"".equals(managementSelector.getKeywordFilter()))
		// ? String.format(" and c.businessNeed like '%%%s%%' and c.purpose
		// like '%%%s%%", keyword)
		// : "";
		String sSessionFilter = (managementSelector.getUseSessionFiltering() && sessionComboSelected != null)
				? String.format(" and c.session.id=%s ", sessionComboSelected.getId()) : "";
		String sStatusFilter = (managementSelector.getUseStatusFiltering() && statusComboSelected != null)
				? String.format(" and c.originator.id=%s", statusComboSelected.getId()) : "";
		String sOriginatorFilter = (managementSelector.getUseOriginatorFiltering() && originatorComboSelected != null)
				? String.format(" and c.status.id=%s", originatorComboSelected.getId()) : "";
		String keywordContent = managementSelector.getKeywordFilter();
		String sKeyword = (keywordContent != null && !"".equals(keywordContent)) ? String.format(
				" and (c.businessNeed like '%%%s%%' or c.purpose like '%%%s%%' or c.example like '%%%s%%') ",
				keywordContent, keywordContent, keywordContent) : "";
 
		logger.debug("query search: " + q);
		try {
			dancerList = crudService.query(Dancer.class,pagingModel.query( "select c", "select count(*)", " from Dancer c ",
					" LEFT JOIN FETCH c.submitter LEFT JOIN FETCH c.originator LEFT JOIN FETCH c.status st LEFT JOIN FETCH st.code "
							+ " LEFT JOIN FETCH c.session ",
							" where 1=1 %s %s %s", sSessionFilter, sStatusFilter, sOriginatorFilter,sKeyword).count());
			return dancerList;
		} catch (Exception e) {
			dancerList = null;
			logger.error(String.format("alert('error search dancer: %s %s')", e.getMessage(), q));
			//Clients.evalJavaScript(String.format("alert('error search dancer: %s %s')", e.getMessage(), q));
			return new ArrayList<>();
		}
	}
	
	@NotifyChange({ "dancerList", "pagingModel" })
	@Command
	public List<Dancer> paging() {
		dancerList = crudService.query(Dancer.class, pagingModel.dontCount());
		return dancerList;
	}

	public Dancer getSelected() {
		return selected;
	}

	public void setSelected(Dancer selected) {
		this.selected = selected;
	}

	public List<Dancer> getDancerList() {
		return dancerList;
	}

	public void setDancerList(List<Dancer> dancerList) {
		this.dancerList = dancerList;
	}

	public DancerManagementSelector getManagementSelector() {
		return managementSelector;
	}

	public void setManagementSelector(DancerManagementSelector managementSelector) {
		this.managementSelector = managementSelector;
	}

	public List<MeetingSession> getSessionCombo() {
		return sessionCombo;
	}

	public void setSessionCombo(List<MeetingSession> sessionCombo) {
		this.sessionCombo = sessionCombo;
	}

	public MeetingSession getSessionComboSelected() {
		return sessionComboSelected;
	}

	public void setSessionComboSelected(MeetingSession sessionComboSelected) {
		this.sessionComboSelected = sessionComboSelected;
	}

	public List<Code> getOriginatorCombo() {
		return originatorCombo;
	}

	public void setOriginatorCombo(List<Code> originatorCombo) {
		this.originatorCombo = originatorCombo;
	}

	public Code getOriginatorComboSelected() {
		return originatorComboSelected;
	}

	public void setOriginatorComboSelected(Code originatorComboSelected) {
		this.originatorComboSelected = originatorComboSelected;
	}

	public List<Code> getStatusCombo() {
		return statusCombo;
	}

	public void setStatusCombo(List<Code> statusCombo) {
		this.statusCombo = statusCombo;
	}

	public Code getStatusComboSelected() {
		return statusComboSelected;
	}

	public void setStatusComboSelected(Code statusComboSelected) {
		this.statusComboSelected = statusComboSelected;
	}

	public PagingModel getPagingModel() {
		return pagingModel;
	}

	public void setPagingModel(PagingModel pagingModel) {
		this.pagingModel = pagingModel;
	};

}
