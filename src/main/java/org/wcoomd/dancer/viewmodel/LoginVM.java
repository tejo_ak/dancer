package org.wcoomd.dancer.viewmodel;

import org.wcoomd.dancer.springbean.AppUtil;
import org.wcoomd.dancer.springbean.ApplicationInit;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;

@VariableResolver(DelegatingVariableResolver.class)
public class LoginVM {
	private String username="tejo.kusuma@wcoomd.org";
	private String password="passw0rd";
	private String loginMessage;

	@WireVariable
	AppUtil appUtil;

	@WireVariable
	ApplicationInit applicationInit;
	
	@Init
	public void init() {

	}

	@NotifyChange("loginMessage")
	@Command
	public void login() {
		loginMessage = null;
		if (!appUtil.login(username, password)) {
			loginMessage = "your user name or password is invalid";
		}
	}
	@NotifyChange("loginMessage")
	@Command
	public void initApp() {
		applicationInit.initAll();
		Clients.showNotification("application initialization has been completed");
	}

	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getLoginMessage() {
		return loginMessage;
	}

	public void setLoginMessage(String loginMessage) {
		this.loginMessage = loginMessage;
	}
	

}
