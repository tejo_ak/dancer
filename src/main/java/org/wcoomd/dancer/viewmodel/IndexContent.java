package org.wcoomd.dancer.viewmodel;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wcoomd.dancer.model.User;
import org.wcoomd.dancer.springbean.AppUtil;
import org.wcoomd.dancer.springbean.ApplicationConstant;
import org.wcoomd.dancer.springbean.ApplicationInit;
import org.wcoomd.dancer.springbean.CrudService;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.Include;
import org.zkoss.zul.South;

@VariableResolver(DelegatingVariableResolver.class)
public class IndexContent {
	Logger logger = LoggerFactory.getLogger(IndexContent.class);
	private User activeUser;
	private String sessionId;

	@Listen("onRoute = #inclMain")
	public void onSrc(Event evt) {
		inclMain.setSrc(null);
		if (activeUser != null) {
			inclMain.setDynamicProperty("userId", activeUser.getId().toString());
		}
		if (evt.getData() instanceof JSONObject) {
			JSONObject jdata = (JSONObject) evt.getData();
			inclMain.setDynamicProperty("codeType", jdata.get("codeType"));
			logger.debug("path to go:" + (String) jdata.get("path"));
			inclMain.setSrc((String) jdata.get("path"));
			return;
		}
		if (evt.getData() instanceof String) {

			inclMain.setSrc((String) evt.getData());
		}
	}

	@Listen("onRoute = #inclSouth")
	public void onSouthSrc(Event evt) {
		// if (evt.getData() instanceof String) {
		// panelSouth.setHeight("300px");
		// inclSouth.setSrc((String) evt.getData());
		// }
		inclSouth.setSrc(null);
		logger.debug("evt data " + evt.getData());
		if (evt.getData() instanceof JSONObject) {
			JSONObject jdata = (JSONObject) evt.getData();
			// panelSouth.setHeight("300px");
			String param = (jdata.get("param") != null) ? (String) jdata.get("param") : "";
			param = (param != null && !"".equals(param)) ? param : "?dancerId=" + jdata.get("dancerId");
			inclSouth.setDynamicProperty("dancerId", jdata.get("dancerId"));
			inclSouth.setDynamicProperty("callerType", jdata.get("callerType"));
			inclSouth.setDynamicProperty("resourceStream", jdata.get("resourceStream"));
			inclSouth.setDynamicProperty("resource", jdata.get("resource"));
			String path = (jdata != null && jdata.get("path") != null) ? ((String) jdata.get("path") + param) : "";
			logger.debug("final path: " + path);
			inclSouth.setSrc(path);

		}
	}

	@WireVariable
	ApplicationInit applicationInit;

	@WireVariable
	AppUtil appUtil;

	@Listen("onInitApplication = #inclMain")
	public void onInitApplication(Event evt) {
		applicationInit.initAll();
		Clients.showNotification("application initialization has been completed");
	}

	@Init
	public void init() {
		Object oid = Executions.getCurrent().getSession().getAttribute(ApplicationConstant.USER_SESSION_ATTRIBUTE);
		if (oid == null) {
			// no session found from the login
			appUtil.logout();
			return;
		}
		List<User> users = crudService.query(User.class, "select u from User u where u.id=%s", (String) oid.toString());
		if (users != null && users.size() > 0) {
			activeUser = users.get(0);
		}
	}

	@WireVariable
	CrudService crudService;

	@Wire()
	Include inclMain, inclSouth;

	@Wire
	South panelSouth;

	@AfterCompose
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		Selectors.wireEventListeners(view, this);
	}

	public User getActiveUser() {
		return activeUser;
	}

	public void setActiveUser(User activeUser) {
		this.activeUser = activeUser;
	}

	@Command
	public void logout() {
		logger.debug("loging out now");
		appUtil.logout();
		// place
		// Executions.sendRedirect(ApplicationConstant.LOGIN_ZUL);
	}

}
