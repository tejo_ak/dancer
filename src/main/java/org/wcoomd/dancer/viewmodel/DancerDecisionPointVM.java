package org.wcoomd.dancer.viewmodel;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wcoomd.dancer.model.Annotation;
import org.wcoomd.dancer.model.Code;
import org.wcoomd.dancer.model.Dancer;
import org.wcoomd.dancer.model.DancerStatus;
import org.wcoomd.dancer.model.DecisionSupport;
import org.wcoomd.dancer.springbean.AppUtil;
import org.wcoomd.dancer.springbean.ApplicationConstant;
import org.wcoomd.dancer.springbean.CrudService;
import org.wcoomd.dancer.springbean.DancerService;
import org.wcoomd.dancer.springbean.Trailler;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;

@VariableResolver(DelegatingVariableResolver.class)
public class DancerDecisionPointVM {
	Logger logger = LoggerFactory.getLogger(DancerDecisionPointVM.class);
	private Dancer dancer;
	List<DecisionSupport> supportList;
	DecisionSupport support;
	private DancerStatus status;
	private DancerStatus statusBuffer;
	private String callerType = "";// type of caller requesting this form,
									// either management or decision
	private String decisionNote = "";
	private String decisionTypes = ApplicationConstant.DECISION_TYPE_DECISION;
	private boolean decided = false;
	private boolean anyDancer = false;
	private String undecidableMessage;// reason why decision can't be taken at
										// this point
	private boolean decidable = false;// switch if button decide clickable or
										// not based on several measures
	private boolean supportable = false;// switch if supports is required for
										// certain type of decision
	//
	private List<Code> memberCombo;
	Code member;
	private List<Code> decisionCombo;
	Code decision;

	@WireVariable
	CrudService crudService;

	@WireVariable
	Trailler trailler;

	@WireVariable
	AppUtil appUtil;

	@WireVariable
	DancerService dancerService;

	@Init()
	public void init(@ExecutionArgParam("dancerId") String dancerId,
			@ExecutionArgParam("callerType") String callerType) {
		if (dancerId != null) {
			dancer = dancerService.getDancer(dancerId);
			// check if decision already taken and it is approval!
			this.callerType = callerType;
			logger.debug("caller Type:" + callerType);
			this.decisionTypes = (ApplicationConstant.DECISION_CALLER_TYPE_MANAGEMENT.equals(callerType))
					? ApplicationConstant.DECISION_TYPE_MANAGEMENT : ApplicationConstant.DECISION_TYPE_DECISION;
			decisionCombo = getDecisionComboItem();
			decision = dancer.getStatus().getCode();
			memberCombo = getMembers();
			if (dancer.getStatus() == null || (dancer.getStatus() != null
					&& ApplicationConstant.CODE_STATUS_TYPE_SUBMITED.equals(dancer.getStatus().getCode().getCode()))) {
				undecidableMessage = "No decision has been choosen";
				decidable = false;
				decision = null;
				return;
			} else if (dancer.getStatus() != null
					&& ApplicationConstant.CODE_STATUS_TYPE_APPROVED.equals(dancer.getStatus().getCode().getCode())) {
				decidable = false;
				decided = true;
				supportable = true;
				undecidableMessage = "DMR already approved";
				decision = dancer.getStatus().getCode();
				supportList = revealOfDancer(dancer);
				return;
			} else if (dancer.getStatus() != null
					&& !ApplicationConstant.CODE_STATUS_TYPE_SUBMITED.equals(dancer.getStatus().getCode().getCode())) {
				decidable = false;
				decided = true;
				decision = dancer.getStatus().getCode();
				undecidableMessage = "A Decision already taken for the DMR";
				return;
			}

		}
		undecidableMessage = "there is not any DMR has been selected, decision cannot be taken!";
		decidable = false;// with out any dancer exists or selected, it is not
							// decidable
	}

	@NotifyChange({ "supportList", "decision", "decided" })
	@Command
	public void save() {
		Messagebox.show(
				String.format("Decision for the selected DMR will be recorded!", dancer.getId(),
						dancer.getUserReference()),
				"Confirm decision", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new EventListener<Event>() {

					@Override
					public void onEvent(Event event) throws Exception {
						// TODO Auto-generated method stub
						if (event != null && "onOK".equals(event.getName())) {
							if (decision != null && dancer != null) {
								logger.debug("check decision and dancer " + dancer + " " + decision);
								if (decision.getId() != null) {
									supportable = false;
									if ("APPROVED".equals(decision.getCode())) {
										status = dancer.getSupportBuffer();
										dancer.setSupportBuffer(null);
										dancer.setStatus(status);
										status = trailler.trailingStatus(null, status, Trailler.TRAIL_TYPE_CODE_UPDATE);
										crudService.save(dancer);
									} else {
										status = new DancerStatus();
										status.setCode(decision);
										status.setDancer(dancer);
										dancer.setSupportBuffer(null);
										dancer.setStatus(status);
										status.setNote(decisionNote);
										dancer.setSupportBuffer(null);
										status = trailler.trailingStatus(null, status, Trailler.TRAIL_TYPE_CODE_CREATE);
										crudService.save(dancer);
										if (supportList != null) {
											for (DecisionSupport support : supportList) {
												// cleanup unused support
												crudService.delete(support);
											}
										}
									}
									decided = true;
									decidable = false;
									undecidableMessage = "conggratulation, decision has been made for this DMR.";
								}
								Clients.showNotification("Decision succesfully recorded");
								BindUtils.postNotifyChange(null, null, DancerDecisionPointVM.this, "supportList");
								BindUtils.postNotifyChange(null, null, DancerDecisionPointVM.this, "decision");
								BindUtils.postNotifyChange(null, null, DancerDecisionPointVM.this, "decided");
							}

						}
					}
				});

	}

	@NotifyChange({ "supportList", "decision", "supportable", "decidable", "undecidableMessage" })
	@Command
	public void selectDecision() {
		decidable = true;
		undecidableMessage = "";
		if (decision != null && "APPROVED".equals(decision.getCode())) {
			supportList = revealOfDancer(dancer);
			supportable = true;
			//
			decidable = false;
			undecidableMessage = "number of Members' support is not enough to make decision";
			if (supportList != null && supportList.size() > 2) {
				decidable = true;
				undecidableMessage = "";

			}
			return;
		}
		supportable = false;
	}

	@NotifyChange({ "supportList", "decidable", "undecidableMessage" })
	@Command
	public void deleteSupport(@BindingParam("support") DecisionSupport support) {
		if (support != null) {
			crudService.delete(support);
			supportList = revealOfDancer(dancer);

		}
		afterSupport();
	}

	@NotifyChange({ "supportList", "member", "decidable", "undecidableMessage" })
	@Command
	public void saveSupport() {
		logger.debug(
				"save supports? " + dancer + member + ((member != null) ? "--" + member.getId() : "member no id"));
		if (dancer != null && member != null) {
			if (member.getId() != null) {
				// initialized bufferStatus if it has not been set
				statusBuffer = dancer.getSupportBuffer();
				logger.debug("status buffer= " + statusBuffer);

				if (statusBuffer == null) {
					Code approved = getStatusType("APPROVED");
					statusBuffer = new DancerStatus();
					statusBuffer.setCode(approved);
					statusBuffer.setDancer(dancer);
					statusBuffer = crudService.save(statusBuffer);
					logger.debug("after save buffer: " + statusBuffer);
					dancer.setSupportBuffer(statusBuffer);
					crudService.save(dancer);
					// statusBuffer = trailler.trailingStatus(null,
					// statusBuffer, Trailler.TRAIL_TYPE_CODE_CREATE);
				}

				support = new DecisionSupport();
				support.setMember(member);
				support.setStatus(statusBuffer);
				crudService.save(support);
				supportList = revealOfDancer(dancer);
				member = null;
			}
			afterSupport();

		}
	}

	private void afterSupport() {
		if (supportList != null && supportList.size() >= 2) {// number of
			// minimum
			// vote
			// could be
			// managed
			// centraly
			decidable = true;
			undecidableMessage = "";
		} else {
			decidable = false;
			undecidableMessage = "Not enough Members' support to make decision";
		}
	}

	public Code getStatusType(String type) {
		List<Code> codes = crudService.query(Code.class,
				"select c from Code c where c.code='%s' and c.type.code='DANCER_STATUS_TYPE'", type);
		return codes.size() > 0 ? codes.get(0) : null;
	}

	public List<Code> getMembers() {
		List<Code> codes = crudService.query(Code.class,
				"select c from Code c where  c.group.code='%s' and c.type.code='%s'",
				ApplicationConstant.CODE_MEMBER_TYPE_MEMBER, ApplicationConstant.CODE_TYPE_ORIGINATOR);
		logger.debug("jumlah members: " + codes.size());
		return codes;
	}

	public List<Code> getDecisionComboItem() {
		List<Code> codes = crudService.query(Code.class,
				"select c from Code c where  c.type.code='DANCER_STATUS_TYPE' and '%s' like concat('%%',c.code,'%%')",
				decisionTypes);
		return codes;
	}

	public List<DecisionSupport> revealOfDancer(Dancer dancer) {
		if (dancer == null) {
			logger.debug("dancer cannot be found, therefore exit");
			return new ArrayList<>();
		}
		DancerStatus whichStatusSupport = dancer.getSupportBuffer() == null ? dancer.getStatus()
				: dancer.getSupportBuffer();
		logger.debug("id dancer status=" + whichStatusSupport.getId());
		if (dancer != null && dancer.getId() != null) {
			return crudService.query(DecisionSupport.class,
					String.format(
							"select a from DecisionSupport a LEFT JOIN FETCH a.member LEFT JOIN FETCH a.status  where a.status.id=%s",
							whichStatusSupport.getId()));
		}
		return new ArrayList<>();
	}

	public Dancer getDancer() {
		return dancer;
	}

	public void setDancer(Dancer dancer) {
		this.dancer = dancer;
	}

	public List<DecisionSupport> getSupportList() {
		return supportList;
	}

	public void setSupportList(List<DecisionSupport> supportList) {
		this.supportList = supportList;
	}

	public DecisionSupport getSupport() {
		return support;
	}

	public void setSupport(DecisionSupport support) {
		this.support = support;
	}

	public String getDecisionNote() {
		return decisionNote;
	}

	public void setDecisionNote(String decisionNote) {
		this.decisionNote = decisionNote;
	}

	public boolean isDecided() {
		return decided;
	}

	public void setDecided(boolean decided) {
		this.decided = decided;
	}

	public boolean isDecidable() {
		return decidable;
	}

	public void setDecidable(boolean decidable) {
		this.decidable = decidable;
	}

	public List<Code> getMemberCombo() {
		return memberCombo;
	}

	public void setMemberCombo(List<Code> memberCombo) {
		this.memberCombo = memberCombo;
	}

	public List<Code> getDecisionCombo() {
		return decisionCombo;
	}

	public void setDecisionCombo(List<Code> decisionCombo) {
		this.decisionCombo = decisionCombo;
	}

	public void setDecision(Code decision) {
		this.decision = decision;
	}

	public String getUndecidableMessage() {
		return undecidableMessage;
	}

	public void setUndecidableMessage(String undecidableMessage) {
		this.undecidableMessage = undecidableMessage;
	}

	public Code getDecision() {
		return decision;
	}

	public Code getMember() {
		return member;
	}

	public void setMember(Code member) {
		this.member = member;
	}

	public boolean isSupportable() {
		return supportable;
	}

	public void setSupportable(boolean supportable) {
		this.supportable = supportable;
	}

}
