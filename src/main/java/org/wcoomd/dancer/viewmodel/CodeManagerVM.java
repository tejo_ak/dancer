package org.wcoomd.dancer.viewmodel;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.asm.commons.TryCatchBlockSorter;
import org.wcoomd.dancer.model.Code;
import org.wcoomd.dancer.model.Dancer;
import org.wcoomd.dancer.model.DancerStatus;
import org.wcoomd.dancer.model.MeetingSession;
import org.wcoomd.dancer.model.Trail;
import org.wcoomd.dancer.springbean.AppUtil;
import org.wcoomd.dancer.springbean.ApplicationConstant;
import org.wcoomd.dancer.springbean.CrudService;
import org.wcoomd.dancer.springbean.DancerService;
import org.wcoomd.dancer.springbean.PagingModel;
import org.wcoomd.dancer.springbean.Trailler;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.ExecutionArgParam;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zhtml.Messagebox;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;

@VariableResolver(DelegatingVariableResolver.class)
public class CodeManagerVM {
	Logger logger = LoggerFactory.getLogger(CodeManagerVM.class);
	Code edited;
	Code selected;
	//
	boolean deletable = false;
	boolean savable = true;
	boolean copyable = true;
	boolean submitable = false;

	PagingModel pagingModel = new PagingModel(10);
	List<Code> codeList;
	// Filter
	List<Code> codeTypeCombo;
	Code codeTypeFilterComboSelected;
	boolean useCodeTypeFilter = true;
	//
	// codeGroupCombo
	List<Code> codeGroupCombo;
	Code codeGroupComboSelected;
	//
	String keyword;

	//
	// group combo
	List<Code> groupCombo;
	Code groupComboSelected;
	//
	@WireVariable
	CrudService crudService;

	@WireVariable
	AppUtil appUtil;

	@WireVariable
	Trailler trailler;

	@AfterCompose
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
	}

	@Init
	public void init(@ExecutionArgParam("codeType") String codeType) {
		edited = new Code();
		logger.debug("codeType "+codeType);
		if (codeType != null) {
			if (codeType.equals(ApplicationConstant.CODE_TYPE_ORIGINATOR)) {
				List<Code> s = crudService.query(Code.class,
						"select c from Code c where c.type is null and c.code='%s'",
						ApplicationConstant.CODE_TYPE_ORIGINATOR);
				codeTypeCombo = s;
				codeTypeFilterComboSelected = s.get(0);
				return;
			}
		}
		codeTypeCombo = crudService.query(Code.class, "select c from Code c where c.type is null");
		// groupCombo = getCodeComboList(ApplicationConstant.CODE_TYPE_DATASET);
	};

	private void checkCrudButtonStatus() {
		// savable = (edited != null
		// && ((edited.getStatus() != null && String.format("%s",
		// ApplicationConstant.CODE_STATUS_TYPE_DRAFT)
		// .contains(edited.getStatus().getCode().getCode()))
		// || (edited.getStatus() == null && edited.getId() == null)));
		// submitable = savable;
		// copyable = (edited != null && (edited.getStatus() != null && String
		// .format("%s-%s-%s-%s-%s", ApplicationConstant.CODE_STATUS_TYPE_DRAFT,
		// ApplicationConstant.CODE_STATUS_TYPE_WITHDRAWN,
		// ApplicationConstant.CODE_STATUS_TYPE_SUBMITED,
		// ApplicationConstant.CODE_STATUS_TYPE_APPROVED,
		// ApplicationConstant.CODE_STATUS_TYPE_DEFFERED)
		// .contains(edited.getStatus().getCode().getCode())));
		// deletable = (edited != null && (edited.getStatus() != null && String
		// .format("%s-%s-%s", ApplicationConstant.CODE_STATUS_TYPE_DRAFT,
		// ApplicationConstant.CODE_STATUS_TYPE_WITHDRAWN,
		// ApplicationConstant.CODE_STATUS_TYPE_DEFFERED)
		// .contains(edited.getStatus().getCode().getCode())));
	}

	@NotifyChange({ "edited", "dancerList", "savable", "copyable", "deletable", "submitable" })
	@Command
	public Code save() {
		edited.setType(codeTypeFilterComboSelected);
		edited = crudService.save(edited);
		edited = crudService.query(Code.class, "select c from Code c where c.id=%s", edited.getId().toString()).get(0);
		Clients.showNotification(String.format("The Code  is saved successfully! %s", ""));
		checkCrudButtonStatus();
		codeList = paging();
		return edited;
	}

	@NotifyChange({ "edited", "dancerList" })
	@Command
	public void submit() {

	}

	@NotifyChange({ "edited", "savable", "deletable" })
	@Command
	public void newCode() {
		edited = new Code();
		savable = true;
	}

	@NotifyChange({ "edited", "dancerList", "savable", "copyable", "deletable", "submitable" })
	@Command
	public void savethennew() {
		save();
		edited = new Code();
		checkCrudButtonStatus();
	}

	@NotifyChange({ "edited", "dancerList" })
	@Command
	public void delete() {
		Messagebox.show(String.format("The Code with Code type %s will be deleted", edited.getName()),
				"Confirm deletion", Messagebox.OK | Messagebox.CANCEL, Messagebox.QUESTION, new EventListener<Event>() {

					@Override
					public void onEvent(Event event) throws Exception {
						// TODO Auto-generated method stub
						if (event != null && "onOK".equals(event.getName())) {

							try {
								crudService.delete(edited);
								codeList = paging();
								checkCrudButtonStatus();
							} catch (Exception e) {
								throw new RuntimeException("Error delete code because:" + e.getMessage(), e);
							}
							Clients.showNotification("Code succesfully deleted");
							BindUtils.postNotifyChange(null, null, CodeManagerVM.this, "edited");
							BindUtils.postNotifyChange(null, null, CodeManagerVM.this, "dancerList");
							BindUtils.postNotifyChange(null, null, CodeManagerVM.this, "savable");
							BindUtils.postNotifyChange(null, null, CodeManagerVM.this, "copyable");
							BindUtils.postNotifyChange(null, null, CodeManagerVM.this, "submitable");
							BindUtils.postNotifyChange(null, null, CodeManagerVM.this, "deletable");
						}

					}
				});

	}

	public List<Code> getCodeComboList(String typeCode) {
		return crudService.query(Code.class, "select c from Code c where c.type.code='%s'", typeCode);
	}

	@NotifyChange({ "codeList", "pagingModel", "codeGroupCombo" })
	@Command
	public List<Code> search() {
		// try {
		if (codeTypeFilterComboSelected == null) {
			Clients.showNotification("Select a code type please");
			return new ArrayList<>();
		}

		// origin group
		if (ApplicationConstant.CODE_TYPE_ORIGINATOR.equals(codeTypeFilterComboSelected.getCode())) {
			codeGroupCombo = getCodeComboList(ApplicationConstant.CODE_TYPE_MEMBER_GROUP);
		} else {
			codeGroupCombo = new ArrayList<>();
		}

		String sTypeFilter = (useCodeTypeFilter && codeTypeFilterComboSelected != null)
				? String.format(" and c.type.id=%s", codeTypeFilterComboSelected.getId()) : "";
		String keywordFilter = (keyword != null && !"".equals(keyword))
				? String.format("and (c.name like '%%%s%%' or c.code like '%%%s%%' or c.note like '%%%s%%')", keyword,
						keyword, keyword)
				: "";
		codeList = crudService.query(Code.class, pagingModel.query("select c", "select count(*)", " from Code c ",
				" LEFT JOIN FETCH c.type  ", " where 1=1 %s %s ", sTypeFilter, keywordFilter).count());
		logger.debug("apakah errornya sampai di sini?");
		return codeList;
		// } catch (Exception e) {
		// dancerList = null;
		// Clients.evalJavaScript("alert('error search dancer:'+" +
		// e.getMessage() + ")");
		// return new ArrayList<>();
		// }
	}

	@NotifyChange({ "codeList", "pagingModel" })
	@Command
	public List<Code> paging() {
		codeList = crudService.query(Code.class, pagingModel.dontCount());
		return codeList;
	}

	@NotifyChange({ "edited", "originatorComboSelected", "savable", "copyable", "deletable", "submitable" })
	@Command
	public void select() {
		if (this.selected != null) {
			edited = crudService.query(Code.class,
					"select c from Code c LEFT JOIN FETCH c.type LEFT JOIN FETCH c.group where c.id=%s",
					this.selected.getId().toString()).get(0);
			// originatorComboSelected = edited.getOriginator();
			checkCrudButtonStatus();
		}

	}

	@NotifyChange({ "edited", "savable", "copyable", "deletable", "submitable" })
	@Command
	public void copy() {
		if (edited != null && edited.getId() != null) {
			edited.setId(null);
			checkCrudButtonStatus();
		}
	}

	public String concat(String... text) {

		return appUtil.concat(text);
	}

	public Code getEdited() {
		return edited;
	}

	public void setEdited(Code edited) {
		this.edited = edited;
	}

	public Code getSelected() {
		return selected;
	}

	public void setSelected(Code selected) {
		this.selected = selected;
	}

	public boolean isDeletable() {
		return deletable;
	}

	public void setDeletable(boolean deletable) {
		this.deletable = deletable;
	}

	public boolean isSavable() {
		return savable;
	}

	public void setSavable(boolean savable) {
		this.savable = savable;
	}

	public boolean isCopyable() {
		return copyable;
	}

	public void setCopyable(boolean copyable) {
		this.copyable = copyable;
	}

	public PagingModel getPagingModel() {
		return pagingModel;
	}

	public void setPagingModel(PagingModel pagingModel) {
		this.pagingModel = pagingModel;
	}

	public List<Code> getCodeList() {
		return codeList;
	}

	public void setCodeList(List<Code> codeList) {
		this.codeList = codeList;
	}

	public List<Code> getCodeTypeCombo() {
		return codeTypeCombo;
	}

	public void setCodeTypeCombo(List<Code> codeTypeCombo) {
		this.codeTypeCombo = codeTypeCombo;
	}

	public boolean isUseCodeTypeFilter() {
		return useCodeTypeFilter;
	}

	public void setUseCodeTypeFilter(boolean useCodeTypeFilter) {
		this.useCodeTypeFilter = useCodeTypeFilter;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public List<Code> getGroupCombo() {
		return groupCombo;
	}

	public void setGroupCombo(List<Code> groupCombo) {
		this.groupCombo = groupCombo;
	}

	public Code getGroupComboSelected() {
		return groupComboSelected;
	}

	public void setGroupComboSelected(Code groupComboSelected) {
		this.groupComboSelected = groupComboSelected;
	}

	public boolean isSubmitable() {
		return submitable;
	}

	public void setSubmitable(boolean submitable) {
		this.submitable = submitable;
	}

	public Code getCodeTypeFilterComboSelected() {
		return codeTypeFilterComboSelected;
	}

	public void setCodeTypeFilterComboSelected(Code codeTypeFilterComboSelected) {
		this.codeTypeFilterComboSelected = codeTypeFilterComboSelected;
	}

	public List<Code> getCodeGroupCombo() {
		return codeGroupCombo;
	}

	public void setCodeGroupCombo(List<Code> codeGroupCombo) {
		this.codeGroupCombo = codeGroupCombo;
	}

	public Code getCodeGroupComboSelected() {
		return codeGroupComboSelected;
	}

	public void setCodeGroupComboSelected(Code codeGroupComboSelected) {
		this.codeGroupComboSelected = codeGroupComboSelected;
	}

}
