package org.wcoomd.dancer.viewmodel;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wcoomd.dancer.model.Code;
import org.wcoomd.dancer.model.Dancer;
import org.wcoomd.dancer.model.MeetingSession;
import org.wcoomd.dancer.springbean.AppUtil;
import org.wcoomd.dancer.springbean.ApplicationConstant;
import org.wcoomd.dancer.springbean.CrudService;
import org.wcoomd.dancer.springbean.PagingModel;
import org.zkoss.bind.annotation.AfterCompose;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.ContextParam;
import org.zkoss.bind.annotation.ContextType;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.json.JSONObject;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.event.Event;
import org.zkoss.zk.ui.select.Selectors;
import org.zkoss.zk.ui.select.annotation.Listen;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.Wire;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zkplus.spring.DelegatingVariableResolver;
import org.zkoss.zul.Include;

@VariableResolver(DelegatingVariableResolver.class)
public class DancerDecisionVM {
	Logger logger = LoggerFactory.getLogger(DancerDecisionVM.class);
	Dancer selected;
	String routeId = "";
	List<Dancer> dancerList;
	List<MeetingSession> sessionCombo;
	MeetingSession sessionComboSelected;
	List<Code> originatorCombo;
	Code originatorComboSelected;
	String keyword;
	@WireVariable
	CrudService crudService;

	PagingModel pagingModel = new PagingModel(10);

	@WireVariable
	AppUtil appUtil;

	@Listen("onRoute = #inclDecision")
	public void onSrc(Event evt) {
		if (evt.getData() instanceof JSONObject) {
			JSONObject j = (JSONObject) evt.getData();
			Object oPath = j.get("path");
			Object oRouteId = j.get("routeId");
			inclDecision.setSrc(oPath != null ? (String) oPath : "");
			routeId = oRouteId != null ? (String) oRouteId : "";
		}
		JSONObject j = (JSONObject) evt.getData();
		logger.debug("object json path:" + j.get("path"));
	}

	@Wire
	Include inclDecision;

	public String maxCombine(Long max, String... words) {
		return appUtil.maxCombine(max, words);
	}

	@Init
	public void init() {
		sessionCombo = crudService.query(MeetingSession.class, String.format("select c from MeetingSession c  "));
	}

	@AfterCompose
	public void initSetup(@ContextParam(ContextType.VIEW) Component view) {
		Selectors.wireComponents(view, this, false);
		Selectors.wireEventListeners(view, this);
	}

	@Command
	@NotifyChange({ "originatorCombo" })
	public void selectOriginatorCombo() {
		originatorCombo = selectDancerOriginator(sessionComboSelected);
	}

	public List<Code> selectDancerOriginator(MeetingSession session) {
		if (session == null) {
			return new ArrayList<>();
		}

		List<Code> originator = crudService.query(Code.class,
				String.format("select c from Code c  LEFT JOIN FETCH c.type where c.type.code='%s' "
						+ " and exists (select d from Dancer d where d.session.id=%s and d.originator.id=c.id)" + " ",
				ApplicationConstant.CODE_TYPE_ORIGINATOR, session.getId()));
		return originator;
	}

	@Command
	@NotifyChange({ "dancerList", "pagingModel" })
	public List<Dancer> search() {
		String q = "";
		try {
			String sKeyword = (keyword != null && !"".equals(keyword))
					? String.format(" and (c.code like '%%%s%%' or c.name like '%%%s%%' or c.value like '%%%s%%') ",
							keyword, keyword, keyword)
					: "";

			String sSessionFilter = (sessionComboSelected != null)
					? String.format(" and c.session.id=%s ", sessionComboSelected.getId()) : "";
			String sOriginatorFilter = (originatorComboSelected != null)
					? String.format(" and c.originator.id=%s", originatorComboSelected.getId()) : "";

			String statusFilter = String.format(" and '%s-%s-%s' like concat('%%',c.status.code.code,'%%') ",
					ApplicationConstant.CODE_STATUS_TYPE_SUBMITED, ApplicationConstant.CODE_STATUS_TYPE_WITHDRAWN,
					ApplicationConstant.CODE_STATUS_TYPE_DEFFERED, ApplicationConstant.CODE_STATUS_TYPE_APPROVED);
			q = String.format("select c from Dancer c "
					+ " JOIN FETCH c.submitter JOIN FETCH c.originator LEFT JOIN FETCH c.status st LEFT JOIN FETCH st.code "
					+ " JOIN FETCH c.session where 1=1 %s %s  %s", sSessionFilter, sOriginatorFilter, sKeyword);

			dancerList = crudService.query(Dancer.class,
					pagingModel
							.query("select c", "select count(c)", "from Dancer c",
									" LEFT JOIN FETCH c.submitter LEFT JOIN FETCH c.originator LEFT JOIN FETCH c.status st LEFT JOIN FETCH st.code   LEFT JOIN FETCH c.session ",
									" where 1=1 %s %s %s %s", sSessionFilter, sOriginatorFilter, sKeyword, statusFilter)
							.count());
			logger.debug("q" + q);
			logger.debug("jumlah" + dancerList.size());
			return dancerList;
		} catch (Exception e) {
			dancerList = null;
			Clients.evalJavaScript(String.format("alert('error search dancer: %s %s')", e.getMessage(), q));
			dancerList = new ArrayList<>();
			return dancerList;
		}
	}

	@NotifyChange({ "dancerList", "pagingModel" })
	@Command
	public List<Dancer> paging() {
		dancerList = crudService.query(Dancer.class, pagingModel.dontCount());
		return dancerList;
	}

	@Command
	public void select() {
		routeId = (routeId == null || "".equals(routeId)) ? "dancer_view" : routeId;
		// no decision viewer has been selected, by default will load dancer
		// viewer
		Clients.evalJavaScript(String.format("navigateDecision('%s','dancer_id=%s')", routeId, selected.getId()));

	}

	public Dancer getSelected() {
		return selected;
	}

	public void setSelected(Dancer selected) {
		this.selected = selected;
	}

	public List<Dancer> getDancerList() {
		return dancerList;
	}

	public void setDancerList(List<Dancer> dancerList) {
		this.dancerList = dancerList;
	}

	public List<MeetingSession> getSessionCombo() {
		return sessionCombo;
	}

	public void setSessionCombo(List<MeetingSession> sessionCombo) {
		this.sessionCombo = sessionCombo;
	}

	public MeetingSession getSessionComboSelected() {
		return sessionComboSelected;
	}

	public void setSessionComboSelected(MeetingSession sessionComboSelected) {
		this.sessionComboSelected = sessionComboSelected;
	}

	public List<Code> getOriginatorCombo() {
		return originatorCombo;
	}

	public void setOriginatorCombo(List<Code> originatorCombo) {
		this.originatorCombo = originatorCombo;
	}

	public Code getOriginatorComboSelected() {
		return originatorComboSelected;
	}

	public void setOriginatorComboSelected(Code originatorComboSelected) {
		this.originatorComboSelected = originatorComboSelected;
	}

	public String getKeyword() {
		return keyword;
	}

	public void setKeyword(String keyword) {
		this.keyword = keyword;
	}

	public String getViewing() {
		return routeId;
	}

	public void setViewing(String routeId) {
		this.routeId = routeId;
	}

	public PagingModel getPagingModel() {
		return pagingModel;
	}

	public void setPagingModel(PagingModel pagingModel) {
		this.pagingModel = pagingModel;
	}

}
