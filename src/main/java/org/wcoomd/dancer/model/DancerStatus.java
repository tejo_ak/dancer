package org.wcoomd.dancer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;

@Entity
public class DancerStatus implements Identifiable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@ManyToOne(targetEntity = Dancer.class, fetch = FetchType.LAZY)
	private Dancer dancer;
	@ManyToOne(targetEntity = Code.class, fetch = FetchType.LAZY)
	private Code code;
	@ManyToOne(targetEntity = Trail.class, fetch = FetchType.LAZY)
	private Trail trail;
	@Column(length=4064)
	private String note;
	@Column(length=4064)
	private String controlNote;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Dancer getDancer() {
		return dancer;
	}

	public void setDancer(Dancer dancer) {
		this.dancer = dancer;
	}

	public Code getCode() {
		return code;
	}

	public void setCode(Code code) {
		this.code = code;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Trail getTrail() {
		return trail;
	}

	public void setTrail(Trail trail) {
		this.trail = trail;
	}

	public String getControlNote() {
		return controlNote;
	}

	public void setControlNote(String controlNote) {
		this.controlNote = controlNote;
	}

}
