package org.wcoomd.dancer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Code implements Identifiable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@ManyToOne(targetEntity = Code.class, fetch = FetchType.LAZY)
	private Code type;
	@ManyToOne(targetEntity = Code.class, fetch = FetchType.LAZY)
	private Code group;
	@ManyToOne(targetEntity = Code.class, fetch = FetchType.LAZY)
	private Code protection;
	@Column(length = 32)
	private String code;
	@Column(length = 64)
	private String name;
	@Column(length = 4064)
	private String note;
	@Column(length = 1024)
	private String value;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Code getType() {
		return type;
	}

	public void setType(Code type) {
		this.type = type;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public Code getGroup() {
		return group;
	}

	public void setGroup(Code group) {
		this.group = group;
	}

	public Code getProtection() {
		return protection;
	}

	public void setProtection(Code protection) {
		this.protection = protection;
	}

}
