package org.wcoomd.dancer.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Trail implements Identifiable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Temporal(TemporalType.TIMESTAMP)
	@Column(length = 19)
	private Date activityAt;
	@ManyToOne(targetEntity = User.class, fetch = FetchType.LAZY)
	private User activityBy;
	@ManyToOne(targetEntity = Code.class, fetch = FetchType.LAZY)
	private Code type;
	@ManyToOne(targetEntity = Annotation.class, fetch = FetchType.LAZY)
	private Annotation annotation;
	@ManyToOne(targetEntity = Dancer.class, fetch = FetchType.LAZY)
	private Dancer dancer;
	@ManyToOne(targetEntity = DancerStatus.class, fetch = FetchType.LAZY)
	private DancerStatus dancerStatus;
	@ManyToOne(targetEntity = Code.class, fetch = FetchType.LAZY)
	private Code code;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Date getActivityAt() {
		return activityAt;
	}

	public void setActivityAt(Date activityAt) {
		this.activityAt = activityAt;
	}

	public User getActivityBy() {
		return activityBy;
	}

	public void setActivityBy(User activityBy) {
		this.activityBy = activityBy;
	}

	public Code getType() {
		return type;
	}

	public void setType(Code type) {
		this.type = type;
	}

	public Annotation getAnnotation() {
		return annotation;
	}

	public void setAnnotation(Annotation annotation) {
		this.annotation = annotation;
	}

	public Dancer getDancer() {
		return dancer;
	}

	public void setDancer(Dancer dancer) {
		this.dancer = dancer;
	}

	public DancerStatus getDancerStatus() {
		return dancerStatus;
	}

	public void setDancerStatus(DancerStatus dancerStatus) {
		this.dancerStatus = dancerStatus;
	}

	public Code getCode() {
		return code;
	}

	public void setCode(Code code) {
		this.code = code;
	}

}
