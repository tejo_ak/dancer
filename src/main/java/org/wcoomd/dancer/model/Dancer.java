package org.wcoomd.dancer.model;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

@Entity
public class Dancer implements Identifiable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@Column(length=32)
	private String userReference;
	@ManyToOne(targetEntity = User.class, fetch = FetchType.LAZY)
	private User submitter;
	@Column(length=4064)
	private String businessNeed;
	@Column(length=4064)
	private String purpose;
	@Column(length=4064)
	private String example;
	@Temporal(TemporalType.DATE)
	private Date submissionDate;
	@ManyToOne(targetEntity = DancerStatus.class, fetch = FetchType.LAZY)
	private DancerStatus status;
	@ManyToOne(targetEntity = DancerStatus.class, fetch = FetchType.LAZY)
	private DancerStatus supportBuffer;
	@ManyToOne(targetEntity = Code.class, fetch = FetchType.LAZY)
	private Code artefactType;
	@ManyToOne(targetEntity = Code.class, fetch = FetchType.LAZY)
	private Code updateType;
	@ManyToOne(targetEntity = Code.class, fetch = FetchType.LAZY)
	private Code dataset;
	@ManyToOne(targetEntity = Code.class, fetch = FetchType.LAZY)
	private Code procedure;
	@ManyToOne(targetEntity = Code.class, fetch = FetchType.LAZY)
	private Code originator;
	@ManyToOne(targetEntity = MeetingSession.class, fetch = FetchType.LAZY)
	private MeetingSession session;
	private String affectedWcoId;
	@Column(length=12)
	private String untded;
	@Column(length=64)
	private String untdedName;
	@Column(length=1024)
	private String untdedDefinition;

	public Dancer() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserReference() {
		return userReference;
	}

	public void setUserReference(String userReference) {
		this.userReference = userReference;
	}

	public User getSubmitter() {
		return submitter;
	}

	public void setSubmitter(User submitter) {
		this.submitter = submitter;
	}

	public String getBusinessNeed() {
		return businessNeed;
	}

	public void setBusinessNeed(String businessNeed) {
		this.businessNeed = businessNeed;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getExample() {
		return example;
	}

	public void setExample(String example) {
		this.example = example;
	}

	public Date getSubmissionDate() {
		return submissionDate;
	}

	public void setSubmissionDate(Date submissionDate) {
		this.submissionDate = submissionDate;
	}

	public DancerStatus getStatus() {
		return status;
	}

	public void setStatus(DancerStatus status) {
		this.status = status;
	}

	public Code getArtefactType() {
		return artefactType;
	}

	public void setArtefactType(Code artefactType) {
		this.artefactType = artefactType;
	}

	public Code getUpdateType() {
		return updateType;
	}

	public void setUpdateType(Code updateType) {
		this.updateType = updateType;
	}

	public Code getDataset() {
		return dataset;
	}

	public void setDataset(Code dataset) {
		this.dataset = dataset;
	}

	public Code getProcedure() {
		return procedure;
	}

	public void setProcedure(Code procedure) {
		this.procedure = procedure;
	}

	public String getAffectedWcoId() {
		return affectedWcoId;
	}

	public void setAffectedWcoId(String affectedWcoId) {
		this.affectedWcoId = affectedWcoId;
	}

	public String getUntded() {
		return untded;
	}

	public void setUntded(String untded) {
		this.untded = untded;
	}

	public Code getOriginator() {
		return originator;
	}

	public void setOriginator(Code originator) {
		this.originator = originator;
	}

	public MeetingSession getSession() {
		return session;
	}

	public void setSession(MeetingSession session) {
		this.session = session;
	}

	public DancerStatus getSupportBuffer() {
		return supportBuffer;
	}

	public void setSupportBuffer(DancerStatus supportBuffer) {
		this.supportBuffer = supportBuffer;
	}

	public String getUntdedName() {
		return untdedName;
	}

	public void setUntdedName(String untdedName) {
		this.untdedName = untdedName;
	}

	public String getUntdedDefinition() {
		return untdedDefinition;
	}

	public void setUntdedDefinition(String untdedDefinition) {
		this.untdedDefinition = untdedDefinition;
	}

}
