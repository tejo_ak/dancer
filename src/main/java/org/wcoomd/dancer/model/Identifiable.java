package org.wcoomd.dancer.model;

public interface Identifiable {

	public Long getId();

	public void setId(Long id);

}
