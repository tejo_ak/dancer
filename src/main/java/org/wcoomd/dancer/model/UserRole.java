package org.wcoomd.dancer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class UserRole implements Identifiable {
	@Id
	@GeneratedValue
	private Long id;
	@ManyToOne(targetEntity = User.class, fetch = FetchType.LAZY)
	private User user;
	@ManyToOne(targetEntity = Code.class, fetch = FetchType.LAZY)
	private Code role;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public User getUser() {
		return user;
	}
	public void setUser(User user) {
		this.user = user;
	}
	public Code getRole() {
		return role;
	}
	public void setRole(Code role) {
		this.role = role;
	}
	 
	
}
