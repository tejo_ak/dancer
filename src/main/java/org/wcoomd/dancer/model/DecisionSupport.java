package org.wcoomd.dancer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class DecisionSupport implements Identifiable {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	private Long id;
	@ManyToOne(targetEntity = DancerStatus.class, fetch = FetchType.LAZY)
	private DancerStatus status;
	@Column(length=4064)
	private String note;
	@ManyToOne(targetEntity = Code.class, fetch = FetchType.LAZY)
	private Code member;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public DancerStatus getStatus() {
		return status;
	}
	public void setStatus(DancerStatus status) {
		this.status = status;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public Code getMember() {
		return member;
	}
	public void setMember(Code member) {
		this.member = member;
	}
	
	
	

}
