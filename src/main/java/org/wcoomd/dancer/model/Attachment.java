package org.wcoomd.dancer.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

@Entity
public class Attachment implements Identifiable {
	@Id
	@GeneratedValue
	private Long id;
	@ManyToOne(targetEntity = Dancer.class, fetch = FetchType.LAZY)
	private Dancer dancer;
	@Column(length = 256)
	private String path;
	@Column(length = 256)
	private String name;
	@Column(length = 4064)
	private String note;
	public Long getId() {
		return id;
	}
	public void setId(Long id) {
		this.id = id;
	}
	public Dancer getDancer() {
		return dancer;
	}
	public void setDancer(Dancer dancer) {
		this.dancer = dancer;
	}
	public String getPath() {
		return path;
	}
	public void setPath(String path) {
		this.path = path;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
}
