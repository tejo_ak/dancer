package org.wcoomd.dancer.entiview;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import org.wcoomd.dancer.model.Dancer;
import org.wcoomd.dancer.model.Identifiable;

public class DancerView implements Identifiable {

	private Long id;
	private String userReference;
	private String submitter;
	private String submiterEmail;
	private String submiterPhone;
	private String submiterOriginator;
	private String businessNeed;
	private String purpose;
	private String example;
	@Temporal(TemporalType.DATE)
	private Date submissionDate;
	private String status;
	private String artefactType;
	private String updateType;
	private String dataset;
	private String procedure;
	private String affectedWcoId;
	private String untdedId;
	private String untdedName;
	private String untdedNote;

	public DancerView() {
		super();
	}
	
	

 



	public DancerView(Long id, String userReference, String submitter, String submiterEmail, String submiterPhone,
			String submiterOriginator, String businessNeed, String purpose, String example, Date submissionDate,
			String status, String artefactType, String updateType, String dataset, String procedure,
			String affectedWcoId, String untdedId, String untdedName, String untdedNote) {
		super();
		this.id = id;
		this.userReference = userReference;
		this.submitter = submitter;
		this.submiterEmail = submiterEmail;
		this.submiterPhone = submiterPhone;
		this.submiterOriginator = submiterOriginator;
		this.businessNeed = businessNeed;
		this.purpose = purpose;
		this.example = example;
		this.submissionDate = submissionDate;
		this.status = status;
		this.artefactType = artefactType;
		this.updateType = updateType;
		this.dataset = dataset;
		this.procedure = procedure;
		this.affectedWcoId = affectedWcoId;
		this.untdedId = untdedId;
		this.untdedName = untdedName;
		this.untdedNote = untdedNote;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getUserReference() {
		return userReference;
	}

	public void setUserReference(String userReference) {
		this.userReference = userReference;
	}

	public String getSubmitter() {
		return submitter;
	}

	public void setSubmitter(String submitter) {
		this.submitter = submitter;
	}

	public String getSubmiterEmail() {
		return submiterEmail;
	}

	public void setSubmiterEmail(String submiterEmail) {
		this.submiterEmail = submiterEmail;
	}

	public String getSubmiterPhone() {
		return submiterPhone;
	}

	public void setSubmiterPhone(String submiterPhone) {
		this.submiterPhone = submiterPhone;
	}

	public String getSubmiterOriginator() {
		return submiterOriginator;
	}

	public void setSubmiterOriginator(String submiterOriginator) {
		this.submiterOriginator = submiterOriginator;
	}

	public String getBusinessNeed() {
		return businessNeed;
	}

	public void setBusinessNeed(String businessNeed) {
		this.businessNeed = businessNeed;
	}

	public String getPurpose() {
		return purpose;
	}

	public void setPurpose(String purpose) {
		this.purpose = purpose;
	}

	public String getExample() {
		return example;
	}

	public void setExample(String example) {
		this.example = example;
	}

	public Date getSubmissionDate() {
		return submissionDate;
	}

	public void setSubmissionDate(Date submissionDate) {
		this.submissionDate = submissionDate;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getArtefactType() {
		return artefactType;
	}

	public void setArtefactType(String artefactType) {
		this.artefactType = artefactType;
	}

	public String getUpdateType() {
		return updateType;
	}

	public void setUpdateType(String updateType) {
		this.updateType = updateType;
	}

	public String getDataset() {
		return dataset;
	}

	public void setDataset(String dataset) {
		this.dataset = dataset;
	}

	public String getProcedure() {
		return procedure;
	}

	public void setProcedure(String procedure) {
		this.procedure = procedure;
	}

	public String getAffectedWcoId() {
		return affectedWcoId;
	}

	public void setAffectedWcoId(String affectedWcoId) {
		this.affectedWcoId = affectedWcoId;
	}

	public String getUntdedId() {
		return untdedId;
	}

	public void setUntdedId(String untdedId) {
		this.untdedId = untdedId;
	}

	public String getUntdedName() {
		return untdedName;
	}

	public void setUntdedName(String untdedName) {
		this.untdedName = untdedName;
	}

	public String getUntdedNote() {
		return untdedNote;
	}

	public void setUntdedNote(String untdedNote) {
		this.untdedNote = untdedNote;
	}

}
