package org.wcoomd.dancer.springbean;

import java.util.List;

import org.wcoomd.dancer.model.Identifiable;

public interface CrudService {
	public <T extends Identifiable> List<T> query(Class<T> klass, String... queryAndParam);

	public <T extends Identifiable> void delete(List<T> klass);

	public <T extends Identifiable> List<T> query(Class<T> klass, PagingModel model);

	public int execute(String... queryAndParam);

	<T extends Identifiable> List<T> getAll(Class<T> klass);

	<T extends Identifiable> T save(T klass);

	<T extends Identifiable> void delete(T klass);

	<T extends Identifiable> T getById(Class<T> T, Long id);

	public <T > T querySingle(Class<T> klass, String... queryAndParam);

	public <T extends Identifiable> List<T> save(List<T> klass);
}
