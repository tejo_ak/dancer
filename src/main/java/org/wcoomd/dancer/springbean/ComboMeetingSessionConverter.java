package org.wcoomd.dancer.springbean;

import java.util.List;

import org.wcoomd.dancer.model.Code;
import org.wcoomd.dancer.model.MeetingSession;
import org.zkoss.bind.BindContext;
import org.zkoss.bind.Converter;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ListModel;

public class ComboMeetingSessionConverter implements Converter<String, MeetingSession, Component> {

	@Override
	public MeetingSession coerceToBean(String label, Component cp, BindContext arg2) {
		// TODO Auto-generated method stub
		if (cp instanceof Combobox) {
			Combobox cb = (Combobox) cp;
			return (MeetingSession) cb.getSelectedItem().getValue();
		}
		return null;
	}

	@Override
	public String coerceToUi(MeetingSession session, Component combo, BindContext arg2) {
		// TODO Auto-generated method stub
		String sSession = null;
		if (session != null) {
			sSession = session.getName();
		}
		if (combo != null && combo instanceof Combobox) {
			Combobox c = (Combobox) combo;
			List<Comboitem> cis = c.getItems();
			for (Comboitem comboitem : cis) {
				MeetingSession cd = comboitem.getValue();
				if (cd != null && cd.getId() != null && session != null && cd.getId().equals(session.getId())) {
					c.setSelectedItem(comboitem);
				}
			}
		}
		return sSession;
	}

}
