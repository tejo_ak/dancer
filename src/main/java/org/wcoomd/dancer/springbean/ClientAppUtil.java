package org.wcoomd.dancer.springbean;

import javax.servlet.ServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.zkoss.web.servlet.Servlets;
import org.zkoss.zk.ui.Executions;

public class ClientAppUtil {
	static Logger logger = LoggerFactory.getLogger(ClientAppUtil.class);

	public static String maxCombine(Long l, String[] format) {
		AppUtil appUtil = new AppUtil();
		return appUtil.maxCombine(l, format);
	}

	public static boolean checkSession() {

		Object oid = Executions.getCurrent().getSession().getAttribute(ApplicationConstant.USER_SESSION_ATTRIBUTE);
		logger.debug("check session of the user " + oid);
		return (oid != null);
	}

	public static boolean isInternetExplorer() {
		String brw = Servlets.getBrowser((ServletRequest) Executions.getCurrent().getNativeRequest()).toLowerCase();
		logger.debug(String.format("browser detected: %s", brw));
		return brw.contains("ie");
	}
}
