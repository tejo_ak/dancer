package org.wcoomd.dancer.springbean;

import java.util.List;

import org.wcoomd.dancer.model.Identifiable;

public interface CrudDao {
	public <T extends Identifiable> List<T> query(Class<T> klass, String... queryAndParam);

	public <T extends Identifiable> List<T> query(Class<T> klass, PagingModel model);

	public int execute(String... queryAndParam);

	public <T> T querySingle(Class<T> klass, String... queryAndParam);

	public <T extends Identifiable> void delete(List<T> klass);

	<T extends Identifiable> List<T> getAll(Class<T> klass);

	<T extends Identifiable> T save(T klass);

	<T extends Identifiable> void delete(T klass);

	<T extends Identifiable> T getById(Class<T> klass, Long id);

	public <T extends Identifiable> List<T> save(List<T> klass);

}
