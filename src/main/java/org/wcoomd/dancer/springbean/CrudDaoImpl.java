package org.wcoomd.dancer.springbean;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.wcoomd.dancer.model.Identifiable;

@Repository
public class CrudDaoImpl implements CrudDao {
	Logger logger = LoggerFactory.getLogger(CrudDaoImpl.class);
	@Autowired
	SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public <T extends Identifiable> List<T> getAll(Class<T> klass) {
		return getCurrentSession().createQuery("from " + klass.getName()).list();
	}

	@SuppressWarnings("unchecked")
	public <T extends Identifiable> List<T> query(Class<T> klass, String... queryAndParam) {
		String theQuery = combineQuery(queryAndParam);
		try {
			return getCurrentSession().createQuery(theQuery).list();
		} catch (Exception e) {
			throw new RuntimeException("error query : " + theQuery, e);
		}
	}

	private String combineQuery(String... queryAndParam) {
		if (queryAndParam.length > 0) {
			String theQuery = "";
			if (queryAndParam.length > 1) {
				String mainQuery = queryAndParam[0];
				Object[] param = Arrays.copyOfRange(queryAndParam, 1, queryAndParam.length);
				theQuery = String.format(mainQuery, param);
			} else {
				String mainQuery = queryAndParam[0];
				theQuery = mainQuery;
			}
			return theQuery;
		}
		return null;
	}

	@SuppressWarnings("unchecked")
	public <T extends Identifiable> List<T> query(Class<T> klass, PagingModel model) {
		if (model != null) {
			// anticipate when search has not been invoked, paging info normally
			// defined in search has not defined
			// initialize with working data
			if (model.isCount()) {
				// request for total size
				logger.debug("query count: " + model.getQueryCount());
				logger.debug("query: " + model.getQuery());

				List<Long> total = getCurrentSession().createQuery(model.getQueryCount()).list();
				if (total.size() > 0) {
					model.setTotalSize(total.get(0));
				}
				model.dontCount();
				model.setCurentPage(0);
			}

			Query q = getCurrentSession().createQuery(model.getQuery());
			q.setFirstResult(((model.getCurentPage()) * model.getPageSize()));
			q.setMaxResults(model.getPageSize());
			logger.debug("expected frist result: " + model.getCurentPage());
			return q.list();
		}
		return new ArrayList<>();
	}

	public int execute(String... queryAndParam) {
		String theQuery = combineQuery(queryAndParam);
		try {
			return getCurrentSession().createQuery(theQuery).executeUpdate();
		} catch (Exception e) {
			throw new RuntimeException("error query : " + theQuery, e);
		}
	}

	protected final Session getCurrentSession() {
		return sessionFactory.getCurrentSession();
	}

	public <T extends Identifiable> T save(T klass) {
		if (klass != null && (klass.getId() != null && !new Long(0L).equals(klass.getId()))) {
			getCurrentSession().merge(klass);
		} else {
			getCurrentSession().saveOrUpdate(klass);
		}
		getCurrentSession().refresh(klass);
		return klass;
	}

	public <T extends Identifiable> List<T> save(List<T> klass) {
		List<T> hasils = new ArrayList<>();
		for (T t : klass) {
			t = save(t);
			hasils.add(t);
		}
		return hasils;

	}

	public <T extends Identifiable> void delete(T klass) {
		getCurrentSession().delete(klass);
	}

	public <T extends Identifiable> void delete(List<T> klass) {
		for (Iterator iterator = klass.iterator(); iterator.hasNext();) {
			T t = (T) iterator.next();
			delete(t);
		}
	}

	@SuppressWarnings("unchecked")
	public <T extends Identifiable> T getById(Class<T> klass, Long id) {
		List<T> ts = getCurrentSession().createQuery("from " + klass.getName() + " where id=" + id).list();
		return (ts.size() > 0 ? ts.get(0) : null);
	}

	@Override
	@SuppressWarnings("unchecked")
	public <T> T querySingle(Class<T> klass, String... queryAndParam) {
		String theQuery = combineQuery(queryAndParam);

		List<T> tss = getCurrentSession().createQuery(theQuery).list();
		if (tss.size() > 0) {
			return tss.get(0);
		} else {
			return null;
		}
	}
}
