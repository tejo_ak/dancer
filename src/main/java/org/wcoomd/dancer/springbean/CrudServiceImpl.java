package org.wcoomd.dancer.springbean;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.wcoomd.dancer.model.Identifiable;
import org.wcoomd.dancer.viewmodel.DancerManagementVM;

@Service(value = "crudService")
public class CrudServiceImpl implements CrudService {
	Logger logger = LoggerFactory.getLogger(DancerManagementVM.class);
	@Autowired
	private CrudDao crudDao;

	@Transactional
	public <T extends Identifiable> List<T> query(Class<T> klass, String... queryAndParam) {
		return crudDao.query(klass, queryAndParam);
	}

	@Transactional(readOnly = true)
	public <T extends Identifiable> List<T> getAll(Class<T> klass) {
		return crudDao.getAll(klass);
	}

	@Transactional
	public <T extends Identifiable> T save(T klass) {
		return crudDao.save(klass);
	}

	@Transactional
	public <T extends Identifiable> List<T> save(List<T> klass) {
		return crudDao.save(klass);
	}

	@Transactional
	public <T extends Identifiable> void delete(T klass) {
		crudDao.delete(klass);
	}

	@Transactional
	public <T extends Identifiable> T getById(Class<T> klass, Long id) {
		return crudDao.getById(klass, id);
	}

	@Override
	@Transactional
	public <T  > T querySingle(Class<T> klass, String... queryAndParam) {
		// TODO Auto-generated method stub
		return crudDao.querySingle(klass, queryAndParam);
	}

	@Transactional
	@Override
	public <T extends Identifiable> List<T> query(Class<T> klass, PagingModel model) {
		// TODO Auto-generated method stub
		logger.debug("query service search: " + model.getQuery());
		return crudDao.query(klass, model);
	}

	@Transactional
	@Override
	public int execute(String... queryAndParam) {
		// TODO Auto-generated method stub
		return crudDao.execute(queryAndParam);
	}

	@Override
	@Transactional
	public <T extends Identifiable> void delete(List<T> klass) {
		crudDao.delete(klass);

	}
}
