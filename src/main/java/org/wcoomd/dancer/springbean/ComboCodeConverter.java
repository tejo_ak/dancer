package org.wcoomd.dancer.springbean;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wcoomd.dancer.model.Code;
import org.zkoss.bind.BindContext;
import org.zkoss.bind.Converter;
import org.zkoss.zk.ui.Component;
import org.zkoss.zul.Combobox;
import org.zkoss.zul.Comboitem;
import org.zkoss.zul.ListModel;

public class ComboCodeConverter implements Converter<String, Code, Component> {
	Logger logger = LoggerFactory.getLogger(ComboCodeConverter.class);
	@Override
	public Code coerceToBean(String label, Component cp, BindContext arg2) {
		// TODO Auto-generated method stub
		if (cp instanceof Combobox) {
			Combobox cb = (Combobox) cp;
			return (Code) cb.getSelectedItem().getValue();
		}
		return null;
	}

	@Override
	public String coerceToUi(Code code, Component combo, BindContext arg2) {
		// TODO Auto-generated method stub
		String sCode = null;
		if (code != null) {
			sCode = code.getName();
		}
		if (combo != null && combo instanceof Combobox) {
			Combobox c = (Combobox) combo;
			List<Comboitem> cis = c.getItems();
			for (Comboitem comboitem : cis) {
				Code cd = comboitem.getValue();
				if (cd != null && cd.getId() != null && code != null && cd.getId().equals(code.getId())) {
					 c.setSelectedItem(comboitem);
					sCode=comboitem.getLabel();
				}
			}

		}
		return sCode;
	}

}
