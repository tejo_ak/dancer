package org.wcoomd.dancer.springbean;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.apache.commons.codec.digest.Md5Crypt;
import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wcoomd.dancer.model.User;
import org.zkoss.zk.ui.Execution;
import org.zkoss.zk.ui.Executions;

@Service("appUtil")
public class AppUtil {
	Logger logger = LoggerFactory.getLogger(AppUtil.class);
	public String concat(String... words) {
		String s = "";
		for (int i = 0; i < words.length; i++) {
			String string = words[i];
			s += string;
		}
		return s;
	}

	public String combine(String... words) {
		if (words != null && words.length > 0) {
			String result = "";
			if (words.length > 1) {
				String pattern = words[0];
				Object[] param = Arrays.copyOfRange(words, 1, words.length);
				result = String.format(pattern, param);
			} else {
				String pattern = words[0];
				result = pattern;
			}
			try {
				return result;
			} catch (Exception e) {
				throw new RuntimeException("error query : " + result, e);
			}
		}
		return "";
	}

	// THIS STATIC METHOD TO BE USED IN XEL METHOD AS HELPER
	public String maxCombine(Long length, String... patern) {
		AppUtil appUtil = new AppUtil();
		String s = appUtil.combine(patern);
		if (length > 0L) {
			s = s.substring(0, s.length() < length ? s.length() - 0 : length.intValue());
		}
		return s;
	}

	

	@Autowired
	CrudService crudService;

	public boolean login(String user, String password) {
		// List<User> users = crudService.query(User.class, "select u from User
		// u where u.email='%s' and u.password='%s'",
		// user, BCrypt.hashpw(password, BCrypt.gensalt()));
		List<User> users = crudService.query(User.class, "select u from User u where u.email='%s' and u.password='%s'",
				user, password);
		if (users != null && users.size() > 0) {
			logger.debug("login sukses");
			Executions.getCurrent().getSession().setAttribute(ApplicationConstant.USER_SESSION_ATTRIBUTE,
					users.get(0).getId());
			Executions.sendRedirect(ApplicationConstant.INDEX_ZUL);
			return true;
			// Executions.getCurrent().getSession().setAttribute(ApplicationConstant.USER_SESSION_ATTRIBUTE,
			// users.get(0).getId());
			// Executions.getCurrent().getSession().setAttribute(ApplicationConstant.USER_SESSION_ATTRIBUTE,
			// null);
			// Execution exec = Executions.getCurrent();
			// HttpServletResponse response = (HttpServletResponse)
			// exec.getNativeResponse();
			// try {
			// response.sendRedirect(response.encodeRedirectURL(ApplicationConstant.INDEX_ZUL));
			// } catch (IOException e) {
			// throw new RuntimeException("error redirecting page");
			// } // assume
			// exec.setVoided(true); // no need to create UI since redirect will
			// // take
			// return true;
		}
		logger.debug("login nggak sukses");
		return false;
	}

	public void logout() {
		Executions.getCurrent().getSession().setAttribute(ApplicationConstant.USER_SESSION_ATTRIBUTE, null);
		Executions.sendRedirect(ApplicationConstant.LOGIN_ZUL);
		return;
		// Execution exec = Executions.getCurrent();
		// HttpServletResponse response = (HttpServletResponse)
		// exec.getNativeResponse();
		// try {
		// logger.debug("logging out now!");
		// response.sendRedirect(response.encodeRedirectURL(ApplicationConstant.LOGIN_ZUL));
		// } catch (IOException e) {
		// throw new RuntimeException("error redirecting page");
		// } // assume
		// exec.setVoided(true); // no need to create UI since redirect will
		// take
	}
}
