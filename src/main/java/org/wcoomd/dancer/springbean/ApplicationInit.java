package org.wcoomd.dancer.springbean;

import java.util.Date;
import java.util.List;

import org.apache.commons.codec.digest.Md5Crypt;
import org.mindrot.jbcrypt.BCrypt;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wcoomd.dancer.model.Code;
import org.wcoomd.dancer.model.Dancer;
import org.wcoomd.dancer.model.DancerStatus;
import org.wcoomd.dancer.model.MeetingSession;
import org.wcoomd.dancer.model.User;
import org.zkoss.zk.ui.select.annotation.WireVariable;

/**
 * This class will be used to generate initial information
 * 
 * @author pc
 *
 */
@Service(value = "applicationInit")
public class ApplicationInit {
	Logger logger = LoggerFactory.getLogger(ApplicationInit.class);

	@Autowired
	CrudService crudService;
	@Autowired
	Trailler trailler;

	public boolean checkGenerationNeeded() {
		// cek keperluan
		Long jumlahCode = crudService.querySingle(Long.class, "select count(t) from Code t");
		return !(jumlahCode > 0);
	};

	public Code createCode(String code, String name, String note, Code type) {

		return createCode(code, name, note, type, null);
	}

	public Code createMember(String code, String name, String note, Code type, Code group) {

		return createCode(code, name, note, type, null, group);
	}

	public Code createCode(String code, String name, String note, Code type, String value) {
		return createCode(code, name, note, type, null, null);
	}

	public Code createReadonlyCode(String code, String name, String note, Code type, String value) {
		return createCode(code, name, note, type, null, null);
	}

	public Code createCode(String code, String name, String note, Code type, String value, Code group) {
		Code c = new Code();
		c.setCode(code);
		c.setName(name);
		c.setNote(note);
		c.setType(type);
		c.setValue(value);
		c.setGroup(group);
		crudService.save(c);
		return c;
	}

	public void initAll() {
		if (!checkGenerationNeeded()) {
			return;
		}
		Code dancerArtefactType = createCode(ApplicationConstant.CODE_TYPE_ARTEFACT, "Dancer artefact type", "", null);
		Code dancerStatusType = createCode(ApplicationConstant.CODE_TYPE_STATUS, "Dancer status type", "", null);
		Code dancerUpdateType = createCode(ApplicationConstant.CODE_TYPE_UPDATE, "Dancer update type", "", null);
		Code dancerProcedureType = createCode(ApplicationConstant.CODE_TYPE_PROCEDURE, "Dancer procedure type", "",
				null);
		Code dancerDatasetType = createCode(ApplicationConstant.CODE_TYPE_DATASET, "Dancer Dataset type", "", null);
		Code dancerOriginator = createCode(ApplicationConstant.CODE_TYPE_ORIGINATOR, "Dancer Originator", "", null);
		Code dancerAppSettings = createCode("APPLICATION_SETTINGS", "Application Setting", "", null);
		Code dancerMemberType = createCode(ApplicationConstant.CODE_TYPE_MEMBER_GROUP, "Code group for member", "",
				null);
		Code codeProtectionType = createCode(ApplicationConstant.CODE_PROTECTION_TYPE,
				"Code protection type, readonly or editable", "", null);
		Code userRoleType = createCode(ApplicationConstant.USER_ROLE_TYPE, "Code protection type, readonly or editable",
				"", null);
		Code trailType = createCode(ApplicationConstant.CODE_TYPE_TRAIL, "Type of User role",
				"Type of trail including Member, Manager and Observer", null);
		//
		// APPLICATION SETTINGS
		createCode("MANAGEMENT_STATUSES", "Filter DMR Status to be shown in DMR Management list", "", dancerAppSettings,
				"SUBMITED-WITHDRAWN-APPROVED-DEFFERED");
		//
		Code dataELement = createCode("DATA_ELEMENT", "Data Element", "", dancerArtefactType);
		createCode("CLASS", "Class", "", dancerArtefactType);
		createCode("CODE", "Code-list", "", dancerArtefactType);
		//
		createCode("READONLY", "Code protection type readonly", "", codeProtectionType);
		createCode("EDITABLE", "Code protection type readonly", "", codeProtectionType);
		//
		createCode("MANAGER", "User role type manager", "", codeProtectionType);
		createCode("MEMBER", "User role type Member", "", codeProtectionType);
		createCode("OBSERVER", "User role type observer", "", codeProtectionType);
		createCode("GUEST", "User role type Guest", "", codeProtectionType);

		//
		Code member = createCode(ApplicationConstant.CODE_MEMBER_TYPE_MEMBER, "Member member type with right to vote",
				"", dancerMemberType);
		Code observer = createCode(ApplicationConstant.CODE_MEMBER_TYPE_OBSERVER,
				"Member member type without right to vote", "", dancerMemberType);
		Code secretariat = createCode("SECRETARIAT", "Secretariat member type without right to vote", "",
				dancerMemberType);
		//
		createCode("ADD", "Add new artefact", "", dancerUpdateType);
		createCode("DELETE", "Delete existing artefact", "", dancerUpdateType);
		Code modify = createCode("MODIFY", "Modify existing artefact", "", dancerUpdateType);
		//
		Code draft = createCode(ApplicationConstant.CODE_STATUS_TYPE_DRAFT, "Draft", "", dancerStatusType);
		Code submited = createCode(ApplicationConstant.CODE_STATUS_TYPE_SUBMITED, "Submited", "", dancerStatusType);
		Code approved = createCode(ApplicationConstant.CODE_STATUS_TYPE_APPROVED, "Approved", "", dancerStatusType);
		createCode(ApplicationConstant.CODE_STATUS_TYPE_WITHDRAWN, "Withdrawn", "", dancerStatusType);
		createCode(ApplicationConstant.CODE_STATUS_TYPE_DEFFERED, "Deffered", "", dancerStatusType);
		//
		Code declaration = createCode("DECLARATION", "Declaration", "", dancerDatasetType);
		createCode("SAFE", "Safe", "", dancerDatasetType);
		createCode("TIR", "TIR", "", dancerDatasetType);
		createCode("UNESCO", "UNESCO", "", dancerDatasetType);
		createCode("CITES", "CITES", "", dancerDatasetType);
		createCode("OIE", "OiES", "", dancerDatasetType);
		createCode("ATIGA", "ATIGA", "", dancerDatasetType);
		//
		Code imports = createCode("IMP", "Import", "", dancerProcedureType);
		Code ekspor = createCode("EXP", "Export", "", dancerProcedureType);
		createCode("CRE", "Cargo Report Export", "", dancerProcedureType);
		createCode("CRI", "Cargo Report Import", "", dancerProcedureType);
		//
		Code wco = createMember("WCO", "WCO Secretariat", "", dancerOriginator, secretariat);
		createMember("MY", "Malaysia", "", dancerOriginator, member);
		createMember("ID", "Indonesia", "", dancerOriginator, member);
		createMember("NL", "Netherland", "", dancerOriginator, member);
		createMember("US", "United States", "", dancerOriginator, member);
		createMember("CA", "Canada", "", dancerOriginator, member);
		Code eu = createMember("EU", "European Union", "", dancerOriginator, member);
		createMember("UNECE", "WCO UNECE", "", dancerOriginator, observer);
		//
		createCode("CREATE", "Event when information is CREATED", "", trailType);
		createCode("UPDATE", "Event when information is UPDATED", "", trailType);
		createCode("DELETE", "Event when information is DELETE", "", trailType);
		createCode("DEACTIVATE", "Event when information is DEACTIVATED", "", trailType);
		createCode("ACTIVATED", "Event when information is ACTIVATED", "", trailType);
		//
		User tejo = createUser("tejo.kusuma@wcoomd.org", "Tejo KUSUMA", "+62474295285", "passw0rd");
		User donald = createUser("donald.tan@wcoomd.org", "Donald TAN", "+62474295285", "passw0rd");
		User armen = createUser("armen.manukyan@wcoomd.org", "Armen MANUKYAN", "+62474295285", "passw0rd");
		User byoung = createUser("byoung-kwan.bae@wcoomd.org", "Byoung KWAN BAE", "+62474295285", "passw0rd");
		User karo = createUser("karolyn.salcedo@wcoomd.org", "Karolyn SALCEDO", "+62474295285", "passw0rd");
		//
		MeetingSession sept = createMeetingSession("Sept 2015", new Date(), new Date());
		MeetingSession jan = createMeetingSession("Jan 2016", new Date(), new Date());
		//
		DancerStatus dsDraft = createStatus(draft, "dmr is being prepared");
		DancerStatus dsApproved = createStatus(approved, "congratulation, your dmr is approved");
		DancerStatus dsSubmited = createStatus(submited, "congratulation, your dmr is submited");

		createDancer(dataELement, "WCO-001-2015",
				"Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nullam convallis bibendum suscipit",
				"Nam auctor sapien ut bibendum rutrum. Quisque non tortor id purus dapibus luctus sed et sapien",
				"Aliquam consequat sapien felis, sed venenatis risus vehicula vel", wco, imports, declaration,
				dsSubmited, tejo, new Date(), sept);
		createDancer(dataELement, "WCO-002-2015", "Aenean interdum ex bibendum, posuere quam quis, ullamcorper tortor",
				"Suspendisse suscipit feugiat orci, malesuada interdum mauris",
				"Nam hendrerit nibh quis purus malesuada, eget placerat sem tincidunt", eu, imports, declaration,
				dsSubmited, donald, new Date(), sept);
		createDancer(dataELement, "WCO-003-2015", "Integer et rutrum augue",
				"Vestibulum interdum mauris at enim ultrices semper",
				"Aenean porttitor feugiat purus. Proin venenatis eros leo, quis ullamcorper tellus porta ac", eu,
				ekspor, declaration, dsSubmited, byoung, new Date(), jan);
		createDancer(dataELement, "WCO-003-2015", "Cum sociis natoque penatibus et magnis dis parturient montes",
				"nascetur ridiculus mus. Morbi et congue diam. Vivamus interdum auctor venenatis",
				"Donec viverra ligula nec lacus rutrum, nec lobortis dui condimentum", eu, ekspor, declaration,
				dsSubmited, armen, new Date(), jan);
		createDancer(dataELement, "WCO-003-2015", "Integer et rutrum augue",
				"Sed tincidunt mauris et mi tristique placerat", "Pellentesque feugiat commodo quam vel convallis", eu,
				imports, declaration, dsSubmited, tejo, new Date(), jan);
		createDancer(dataELement, "WCO-003-2015",
				"Vestibulum ultricies cursus condimentum. Sed congue velit sit amet nisi aliquet, at hendrerit dui pretium",
				"Phasellus eget est ut nibh porta finibus. Sed elementum nibh sem, et aliquet lorem fringilla ut",
				"Suspendisse vitae commodo leo, eu feugiat lorem. Aliquam erat volutpat", eu, ekspor, declaration,
				dsSubmited, karo, new Date(), jan);
		createDancer(dataELement, "WCO-003-2015", "Integer et rutrum augue",
				"Vivamus id ipsum cursus, tincidunt nulla eu, dictum mi",
				" Lorem ipsum dolor sit amet, consectetur adipiscing elit", eu, imports, declaration, dsSubmited,
				donald, new Date(), jan);
		createDancer(dataELement, "WCO-003-2015", "Nullam id est egestas, volutpat mauris id, fringilla neque",
				"Donec eleifend placerat sapien, eget maximus urna fermentum rhoncus. Nam sed tellus diam",
				"Nunc consectetur, urna a faucibus rhoncus, nunc nunc aliquet turpis", eu, imports, declaration,
				dsSubmited, donald, new Date(), jan);
		createDancer(dataELement, "WCO-003-2015", "non scelerisque leo eros quis felis",
				"Vestibulum interdum mauris at enim ultrices semper",
				"Ut aliquet ut lectus eleifend gravida. Vestibulum id est ac dolor faucibus tincidunt at ut dolor", eu,
				ekspor, declaration, dsDraft, byoung, new Date(), jan);
		createDancer(dataELement, "WCO-003-2015",
				"Fusce gravida, lectus quis iaculis tempor, sem erat mattis orci, vel pellentesque erat quam eget nulla",
				"Etiam egestas imperdiet neque non commodo. Vivamus consequat malesuada lorem vulputate feugiat",
				"Integer consectetur eros lacus", eu, imports, declaration, dsDraft, armen, new Date(), jan);
		createDancer(dataELement, "WCO-003-2015",
				"Vivamus euismod vel est sed viverra. Cras malesuada facilisis elementum",
				"uis pulvinar tortor aliquet et. Nullam eu elementum neque.r",
				"Integer malesuada mauris mi, et tincidunt magna consequat condimentum", eu, ekspor, declaration,
				dsSubmited, karo, new Date(), jan);
		createDancer(dataELement, "WCO-003-2015", "Donec quis magna mi. Mauris viverra eleifend dictum",
				"Sed commodo mollis convallis", "In risus mauris, commodo ornare velit ac, hendrerit tincidunt enim",
				eu, ekspor, declaration, dsDraft, byoung, new Date(), jan);
		createDancer(dataELement, "WCO-003-2015",
				"Nam imperdiet risus sapien, pellentesque ullamcorper felis finibus non",
				"Praesent ultricies sem quis sollicitudin condimentum. Etiam dapibus metus dolor",
				"ut efficitur leo luctus vitae", eu, ekspor, declaration, dsSubmited, armen, new Date(), jan);
		createDancer(dataELement, "WCO-003-2015", "Suspendisse iaculis purus at scelerisque tempor",
				"Duis nec turpis dignissim, ornare mi dictum, facilisis neque",
				"Cras metus neque, aliquam eget sem fringilla, lobortis dictum ante. Fusce laoreet felis ac lacinia tincidunt",
				eu, ekspor, declaration, dsSubmited, karo, new Date(), jan);

	}

	public Dancer createDancer(Code artefactType, String userReference, String businessNeed, String purpose,
			String example, Code originator, Code procedure, Code dataset, DancerStatus status, User submitter,
			Date submissionDate, MeetingSession session) {
		Dancer dancer = new Dancer();
		dancer.setArtefactType(artefactType);
		dancer.setUserReference(userReference);
		dancer.setBusinessNeed(businessNeed);
		dancer.setPurpose(purpose);
		dancer.setExample(example);
		dancer.setOriginator(originator);
		dancer.setProcedure(procedure);
		dancer.setDataset(dataset);
		// dancer.setStatus(status);
		dancer.setSubmitter(submitter);
		dancer.setSubmissionDate(submissionDate);
		dancer.setSession(session);
		dancer = crudService.save(dancer);
		DancerStatus ds = crudService.querySingle(DancerStatus.class,
				"select d from DancerStatus d LEFT JOIN FETCH d.code where d.id=%s", status.getId().toString());
		trailler.trailingStatus(null, dancer, ds.getCode().getCode(), "editing", Trailler.TRAIL_TYPE_CODE_UPDATE);
		return dancer;
	}

	public User createUser(String email, String name, String phone, String password) {
		User u = new User();
		u.setEmail(email);
		u.setName(name);
		u.setPhone(phone);
		// u.setPassword(BCrypt.hashpw(password, BCrypt.gensalt()));
		u.setPassword(password);
		crudService.save(u);
		return u;
	}

	public DancerStatus createStatus(Code code, String note) {
		DancerStatus ds = new DancerStatus();
		ds.setCode(code);
		ds.setNote(note);
		crudService.save(ds);
		return ds;
	}

	public MeetingSession createMeetingSession(String name, Date start, Date end) {
		MeetingSession ses = new MeetingSession();
		ses.setName(name);
		ses.setStart(start);
		ses.setEnd(end);
		crudService.save(ses);
		return ses;
	}

}
