package org.wcoomd.dancer.springbean;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.wcoomd.dancer.model.Identifiable;

public class PagingModel {
	Logger logger = LoggerFactory.getLogger(PagingModel.class);
	private int pageSize;
	private int curentPage = 0;
	private Long totalSize;
	private boolean count;
	private String query;
	private String queryCount;

	public PagingModel(int PageSize) {
		this.pageSize = PageSize;
	}

	public void gotoLastPage() {
		this.curentPage = new Double(Math.floor(totalSize / pageSize)).intValue();
		logger.debug(String.format("goto last page total %s, size %s, last page %s", totalSize, pageSize, curentPage));
	}

	public PagingModel count() {
		this.count = true;
		return this;
	}

	public PagingModel dontCount() {
		this.count = false;
		return this;
	}

	public boolean initiated() {
		return this.getQuery() != null;
	}

	public PagingModel query(String query) {
		this.query = query;
		return this;
	}

	private Long initialDataId;

	public PagingModel initialData(Long id) {
		this.initialDataId = id;
		return this;
	}

	public PagingModel initialData(Identifiable identifiable) {
		if (identifiable != null) {
			this.initialDataId = identifiable.getId();
		}
		return this;
	}

	public PagingModel query(String select, String selectCount, String form, String join, String where, String order,
			Object... arg) {
		this.query = String.format(String.format("%s %s %s %s %s ", select, form, join, where, order), arg);
		this.queryCount = String.format(String.format("%s %s %s ", selectCount, form, where), arg);
		return this;
	}

//	public PagingModel query(String select, String selectCount, String form, String join, String where, Object... arg) {
//		return this.query(select, form, join, where, "", arg);
//	}

	public int getPageSize() {
		return pageSize;
	}

	public void setPageSize(int pageSize) {
		this.pageSize = pageSize;
	}

	public int getCurentPage() {
		return curentPage;
	}

	public void setCurentPage(int curentPage) {
		this.curentPage = curentPage;
	}

	public Long getTotalSize() {
		return totalSize;
	}

	public void setTotalSize(Long totalSize) {
		this.totalSize = totalSize;
	}

	public boolean isCount() {
		return count;
	}

	public String getQuery() {
		return query;
	}

	public void setQuery(String query) {
		this.query = query;
	}

	public String getQueryCount() {
		return queryCount;
	}

	public void setQueryCount(String queryCount) {
		this.queryCount = queryCount;
	}

	public Long getInitialDataId() {
		return initialDataId;
	}

	public void setInitialDataId(Long initialDataId) {
		this.initialDataId = initialDataId;
	}

}
