package org.wcoomd.dancer.springbean;

public interface ApplicationConstant {
	public static final String CODE_TYPE_ORIGINATOR = "ORIGINATOR_TYPE";
	public static final String CODE_TYPE_DATASET = "DANCER_DATASET_TYPE";
	public static final String CODE_TYPE_STATUS = "DANCER_STATUS_TYPE";
	public static final String CODE_TYPE_ARTEFACT = "DANCER_ARTEFACT_TYPE";
	public static final String CODE_TYPE_UPDATE = "DANCER_UPDATE_TYPE";
	public static final String CODE_TYPE_PROCEDURE = "DANCER_PROCEDURE_TYPE";
	public static final String CODE_TYPE_MEMBER_GROUP = "MEMBER_CODE_GROUP";
	public static final String CODE_TYPE_TRAIL = "TRAIL_TIPE";
	public static final String CODE_MEMBER_TYPE_OBSERVER = "OBSERVER";
	public static final String CODE_MEMBER_TYPE_MEMBER = "MEMBER";
	public static final String CODE_STATUS_TYPE_APPROVED = "APPROVED";
	public static final String CODE_STATUS_TYPE_SUBMITED = "SUBMITED";
	public static final String CODE_STATUS_TYPE_WITHDRAWN = "WITHDRAWN";
	public static final String CODE_STATUS_TYPE_DEFFERED = "DEFFERED";
	public static final String CODE_STATUS_TYPE_DRAFT = "DRAFT";
	public static final String LOGIN_ZUL = "/login.zul";
	public static final String INDEX_ZUL = "/index.zul";
	public static final String USER_SALT = "%FKG4534jgk%$dj<>4)(";
	public static final String USER_SESSION_ATTRIBUTE = "USER_SESSION_ATTRIBUTE";
	public static final String DECISION_TYPE_DECISION = "APPROVED-WITHDRAWN-DEFFERED";
	public static final String DECISION_TYPE_MANAGEMENT = "WITHDRAWN";
	public static final String DECISION_CALLER_TYPE_MANAGEMENT = "management";
	public static final String CODE_PROTECTION_TYPE = "CODE_PROTECTION_TYPE";
	public static final String USER_ROLE_TYPE = "USER_ROLE_TYPE";

}
