
package org.wcoomd.dancer.springbean;

import java.util.Date;
import java.util.List;

import javax.persistence.Entity;

import org.hibernate.service.spi.InjectService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wcoomd.dancer.model.Annotation;
import org.wcoomd.dancer.model.Code;
import org.wcoomd.dancer.model.Dancer;
import org.wcoomd.dancer.model.DancerStatus;
import org.wcoomd.dancer.model.Trail;
import org.wcoomd.dancer.model.User;

@Service("trailler")
public class Trailler {
	Logger logger = LoggerFactory.getLogger(Trailler.class);
	private static Code TRAIL_TYPE_CREATE = null;
	//
	public static final String TRAIL_TYPE_CODE_CREATE = "CREATE";
	public static final String TRAIL_TYPE_CODE_UPDATE = "UPDATE";
	public static final String TRAIL_TYPE_CODE_DELETE = "DELETE";
	public static final String TRAIL_TYPE_CODE_ACTIVATE = "ACTIVATE";
	public static final String TRAIL_TYPE_CODE_DEACTIVATE = "DEACTIVATE";
	//
	@Autowired
	CrudService crudService;

	private Code getTrailType(String code) {
		String q = String.format("select c from Code c where c.type.code='TRAIL_TIPE' and c.code='%s'", code);
		List<Code> codeList = crudService.query(Code.class, q);
		if (codeList.size() > 0) {
			return codeList.get(0);
		}
		return null;
	}

	public Annotation trailingAnnotation(User u, Annotation annotation, String type) {
		try {
			Code c = getTrailType(type);
			Trail t = new Trail();
			t.setType(c);
			t.setActivityAt(new Date());
			t.setActivityBy(u);
			t = crudService.save(t);
			annotation.setTrail(t);
			annotation = crudService.save(annotation);
			t.setAnnotation(annotation);
			t = crudService.save(t);
			return annotation;

		} catch (Exception e) {
			String erme = String.format("error applying DMR Status to comments because %s", e.getMessage());
			logger.error(erme, e);
			throw new RuntimeException(erme, e);
		}
	}

	public DancerStatus trailingStatus(User u, DancerStatus status, String type) {
		try {
			Code c = getTrailType(type);
			Trail t = new Trail();
			t.setType(c);
			t.setActivityAt(new Date());
			t.setActivityBy(u);
			t = crudService.save(t);
			status.setTrail(t);
			status = crudService.save(status);
			t.setDancerStatus(status);
			t = crudService.save(t);
			return status;

		} catch (Exception e) {
			String erme = String.format("error applying DMR Status because %s", e.getMessage());
			logger.error(erme, e);
			throw new RuntimeException(erme, e);
		}
	}

	public DancerStatus trailingStatus(User u, Dancer dancer, String statusCode, String statusNote, String type) {
		try {
			DancerStatus status = new DancerStatus();
			if (dancer != null) {
				status.setDancer(dancer);
			}
			List<Code> codes = crudService.query(Code.class,
					"select c from Code c where c.code='%s' and c.type.code='%s'", statusCode,
					ApplicationConstant.CODE_TYPE_STATUS);
			status.setCode(codes.size() > 0 ? codes.get(0) : null);
			status.setNote(statusNote);
			status = crudService.save(status);
			if (dancer != null) {
				dancer.setStatus(status);
				crudService.save(dancer);
			}
			Code c = getTrailType(type);
			Trail t = new Trail();
			t.setType(c);
			t.setActivityAt(new Date());
			t.setActivityBy(u);
			t = crudService.save(t);
			status.setTrail(t);
			status = crudService.save(status);
			t.setDancerStatus(status);
			t = crudService.save(t);
			return status;

		} catch (Exception e) {
			String erme = String.format("error applying DMR Status because %s", e.getMessage());
			logger.error(erme, e);
			throw new RuntimeException(erme, e);
		}
	}
}
