package org.wcoomd.dancer.springbean;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.wcoomd.dancer.model.Dancer;

@Service("dancerService")
public class DancerService {
	Logger logger = LoggerFactory.getLogger(DancerService.class);
	@Autowired
	CrudService crudService;

	public Dancer getDancer(Long id) {
		String q = String.format("select d from Dancer d LEFT JOIN FETCH d.submitter LEFT JOIN FETCH d.status "
				+ " LEFT JOIN FETCH d.originator LEFT JOIN FETCH d.dataset "
				+ " LEFT JOIN FETCH d.supportBuffer LEFT JOIN FETCH d.session "
				+ " LEFT JOIN FETCH d.artefactType LEFT JOIN FETCH d.updateType "
				+ " LEFT JOIN FETCH d.procedure LEFT JOIN FETCH d.artefactType "
				+ " LEFT JOIN FETCH d.status st LEFT JOIN FETCH st.code " + "   where d.id=%s", id);
		List<Dancer> d = crudService.query(Dancer.class, q);
		logger.debug("size of query " + d.size() + " " + q);
		return d.size() > 0 ? d.get(0) : null;
	}

	public Dancer getDancer(String id) {
		if (id != null) {
			return getDancer(new Long(id));
		}
		return null;
	}
}
